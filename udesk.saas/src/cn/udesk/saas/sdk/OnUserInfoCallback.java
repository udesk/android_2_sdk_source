package cn.udesk.saas.sdk;

/**
 * 更新用户信息。
 * 
 * @author lvwz
 * 
 */
public interface OnUserInfoCallback {

	public void onSuccess();

	public void onFail(String message);
}
