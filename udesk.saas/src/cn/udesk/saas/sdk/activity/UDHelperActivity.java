/**
 * 知识点
 * @author xutao
 */
package cn.udesk.saas.sdk.activity;

import android.content.Intent;
import android.os.Bundle;

public class UDHelperActivity extends UDeskBaseActivity   {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
 
        Intent intent = this.getIntent();
        intent.putExtra(UDeskBaseActivity.FRAGMENTS_TAG, UDHelperFragment.class.getName());
        
        Bundle data = new Bundle();
        intent.putExtra(UDeskBaseActivity.DATA_TAG, data);
  
        super.onCreate(savedInstanceState);
    }

}
