/**
 * 消息历史记录
 *
 * @author xutao
 */
package cn.udesk.saas.sdk.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class UDIMDBHelper extends SQLiteOpenHelper {

    private final static String TAG = "UDESK_DBHelper";

    public final static String DATABASE_NAME     = "udesk_im";
    public final static int    DATABASE_VERSION  = 2;

    public final static String TABLE_NAME = "udesk_im_manager";

    /**
     * @since V1
     */
    public final static String COLUMN_ID            = "id";
    public final static String COLUMN_TYPE          = "type";
    public final static String COLUMN_TEXT_URL      = "text_url";
    public final static String COLUMN_THUMBNAIL_PATH   = "thumbnail_path";
    public final static String COLUMN_SEND_STATE   = "send_state";// 0 为默认值，默认为失败， 1为成功

    public final static int RESULT_WAIT =0;
    public final static int RESULT_FAIL =1;
    public final static int RESULT_SUCCESS =2;
    /**
     * 创建数据库表
     */
    public final static String SQL_CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
                    COLUMN_ID + " INTEGER PRIMARY KEY, " +
                    COLUMN_TYPE + " INTEGER, " +
                    COLUMN_SEND_STATE + " INTEGER   DEFAULT "+RESULT_WAIT+"," +
                    COLUMN_TEXT_URL + " VARCHAR, " +
                    COLUMN_THUMBNAIL_PATH + " VARCHAR)";


    public UDIMDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    	int versionTmp =oldVersion;
    	
    	// setp 1: update for version 1
    	if(oldVersion<2){
        	 db.beginTransaction();
             try {
             	db.execSQL("ALTER TABLE "+TABLE_NAME +
                        " ADD COLUMN "+COLUMN_SEND_STATE+" INTEGER  DEFAULT "+RESULT_WAIT+" ;");
                 db.execSQL("UPDATE "+TABLE_NAME+" SET "+COLUMN_SEND_STATE+" = "+RESULT_SUCCESS+";");//旧数据都认为是success的
                 db.setTransactionSuccessful();
             } catch ( Exception ex) {
                 // Old version remains, which means we wipe old data
             } finally {
                 db.endTransaction();
             }
             
             versionTmp =2;//达到版本2的标准
    	}

    	// setp 2: update for version 2 in the future
    }

}
