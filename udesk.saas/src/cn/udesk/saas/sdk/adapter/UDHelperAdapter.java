/**
 * 知识点列表适配器
 *
 * @author xutao
 */
package cn.udesk.saas.sdk.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import cn.udesk.saas.sdk.ResourcesIDLoader;
import cn.udesk.saas.sdk.helper.UDHelperItem;

public class UDHelperAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<UDHelperItem> list = new ArrayList<UDHelperItem>();

    public UDHelperAdapter(Context context) {
        mContext = context;
        
        LAYOUT_ID_ADAPTER = ResourcesIDLoader.getResIdLoaderInstance(mContext).getResLayoutID("udesk_layout_helper_item");
    }

    @Override
    public int getCount() {
        return list.size();
    }

    public void setList(UDHelperItem[] array) {
        list.clear();
        if(array != null && array.length > 0) {
            for(int i = 0; i < array.length; i ++) {
                list.add(array[i]);
            }
            notifyDataSetChanged();
        }
    }

    @Override
    public UDHelperItem getItem(int position) {
        if(position < 0 || position >= list.size()) {
            return null;
        }
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(LAYOUT_ID_ADAPTER, null);
        }

        ((TextView)convertView).setText(list.get(position).subject);;

        return convertView;
    }
    private   int  LAYOUT_ID_ADAPTER  ;

}
