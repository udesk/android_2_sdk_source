package cn.udesk.saas.sdk.common;

public class HttpResult {

	public int code;// 200 是成功
	public int errorType=-1;//-1表示非法值，即不是错误
	public String result;// 成功下才有意义
	public String error;// 失败时才有意义

	@Override
	public String toString() {
		StringBuilder resultSb = new StringBuilder(200).append("[ ").append(HttpResult.class.getName()).append(", this= ")
				.append(this.hashCode()).append(" code:").append(code);
		
		if(code==200){//200是成功
			resultSb.append("   result:").append(result);
		}else{
			resultSb.append(" errorType:").append(errorType).append("  error:").append(error);
		}
		
		return resultSb.append("]").toString();
	}
}
