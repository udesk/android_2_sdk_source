/**
 * 登录
 *
 * @author xutao
 */
package cn.udesk.saas.sdk.chat;

import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import cn.udesk.saas.sdk.activity.HandlerType;
import cn.udesk.saas.sdk.manager.UDUserManager;
import cn.udesk.saas.sdk.utils.UDEnvConstants;
import cn.udesk.saas.sdk.utils.UDHttpUtil;
import cn.udesk.saas.sdk.utils.UDUtils;

public class UDLoginTask extends AsyncTask<String, Integer, String> {

    private Handler mHandler = null;
    private String customerServiceId = null;

    public UDLoginTask(Handler handler) {
        this.mHandler = handler;
    }

    @Override
    protected String doInBackground(String... params) {

        String subDomain = params[0];
        String secretKey = params[1];
        String uid = params[2];
        String nick = params[3];
        customerServiceId = params[4];

        if(TextUtils.isEmpty(nick)) {
            nick = new String(uid);
        }

        uid = uid + "_" + subDomain;

        StringBuilder builder = new StringBuilder();
            builder.append("nick=").append(nick).append("&")
                   .append("user_id=").append(uid).append("&");
  

        String signParams = builder.toString() + secretKey;

        if(UDEnvConstants.isDebugMode) {
            Log.w("UDLoginTask", signParams);
        }

        String sign = UDUtils.MD5(signParams);

        builder.append("sign=").append(sign);

        String url = String.format("http://%s/api/v1/im.json?", UDUserManager.getInstance().getSubDomain());
        url = url + builder.toString();

        if(UDEnvConstants.isDebugMode) {
            Log.w("UDLoginTask", url);
        }

        return UDHttpUtil.get(url);
    }


    @Override
    protected void onPostExecute(String result) {
        if(UDEnvConstants.isDebugMode) {
            Log.w("UDLoginTask", "  Login  result = " + result);
        }
        try {
            if(!TextUtils.isEmpty(result)) {
                JSONObject json = new JSONObject(result);
                int status = json.optInt("status");

                if(status == 0) {
                    if(json.has("user")) {
                        JSONObject user = json.optJSONObject("user");
                        if(user != null) {
                            String name = user.optString("username");
                            String password = user.optString("password");
                            String server = user.optString("server");
                            String roomjid = user.optString("room_jid");

                            UDUserManager.getInstance().setLoginInfo(name, password, server, roomjid);
                        }
                    }

                    if(json.has("token")) {
                        UDUserManager.getInstance().setQiniuToken(json.optString("token"));
                    }

                    if(json.has("online")) {
                        JSONArray olCSArray = json.optJSONArray("online");
                        if(olCSArray != null) {
                            int size = olCSArray.length();
                            if(size > 0) {
                                if(!TextUtils.isEmpty(customerServiceId)) {
                                    // online & 聊过
                                    for(int i = 0; i < size; i ++) {
                                        if(customerServiceId.equals(olCSArray.optString(i))) {
                                            UDUserManager.getInstance().setCustomerServiceId(customerServiceId, true);
                                            mHandler.sendEmptyMessage(HandlerType.TYPE_START_XMMP);
                                            return;
                                        }
                                    }
                                }
                                // online 里面随机选一个
                                int random = Math.abs(new Random(System.currentTimeMillis()).nextInt()) % size;
                                UDUserManager.getInstance().setCustomerServiceId(olCSArray.optString(random), true);

                                if(UDEnvConstants.isDebugMode) {
                                    Log.w("UDLoginTask", "ol = " + olCSArray.optString(random));
                                }

                                mHandler.sendEmptyMessage(HandlerType.TYPE_START_XMMP);
                                return;
                            }
                        }
                    }

                    if(json.has("all")) {
                        JSONArray allCSArray = json.optJSONArray("all");
                        if(allCSArray != null) {
                            int size = allCSArray.length();
                            if(size > 0) {
                                if(!TextUtils.isEmpty(customerServiceId)) {
                                    // all & 聊过
                                    for(int i = 0; i < size; i ++) {
                                        if(customerServiceId.equals(allCSArray.optString(i))) {
                                            UDUserManager.getInstance().setCustomerServiceId(customerServiceId, false);

                                            if(UDEnvConstants.isDebugMode) {
                                                Log.w("UDLoginTask", "all1 = " + customerServiceId);
                                            }

                                            mHandler.sendEmptyMessage(HandlerType.TYPE_START_XMMP);
                                            return;
                                        }
                                    }
                                }

                                // all 里面随机选一个
                                int random = Math.abs(new Random(System.currentTimeMillis()).nextInt()) % size;
                                UDUserManager.getInstance().setCustomerServiceId(allCSArray.optString(random), false);

                                if(UDEnvConstants.isDebugMode) {
                                    Log.w("UDLoginTask", "all2 = " + allCSArray.optString(random));
                                }

                                mHandler.sendEmptyMessage(HandlerType.TYPE_START_XMMP);
                                return;
                            }
                        }
                    }
                }
            }

            mHandler.sendEmptyMessage(HandlerType.TYPE_CUSTOMER);

        } catch(Exception e) {
        }
    }

}
