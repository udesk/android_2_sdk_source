package cn.udesk.saas.sdk.utils;

/**
 * 这个类在开发模式下不会被移除，因此在通过Class.forName("cn.udesk.saas.sdk.utils.UdeskDebug") 可以确认系统是Debug模式<br>
 * 在jar打包过程中，cn.udesk.saas.sdk.utils.UdeskDebug会被移除，那么系统就变成了非Debug模式<br>
 * @author lvwz
 *
 */
final class UdeskDebug {

}
