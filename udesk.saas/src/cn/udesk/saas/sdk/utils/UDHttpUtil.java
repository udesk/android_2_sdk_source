/**
 * 联网请求
 * @author xutao
 */
package cn.udesk.saas.sdk.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import cn.udesk.saas.sdk.UDeskError;
import cn.udesk.saas.sdk.common.HttpResult;

public class UDHttpUtil {

    /**
     * post
     * @param httpUrl
     * @return
     */
    public static void post(String httpUrl, String content,HttpResult result) {

        DefaultHttpClient httpclient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(httpUrl);

        HttpParams params = httpclient.getParams(); // 计算网络超时用
        HttpConnectionParams.setConnectionTimeout(params, 15 * 1000);
        HttpConnectionParams.setSoTimeout(params, 10 * 1000);
       // httpPost.setHeader("Content-Type", "text/plain");
  
            try {
            	StringEntity httpPostEntity = new StringEntity(content, "UTF-8");
            	httpPostEntity.setContentType("application/json");
	            httpPost.setEntity(httpPostEntity);
	            
	            HttpResponse httpResponse = httpclient.execute(httpPost);
	            StatusLine statusLine =httpResponse.getStatusLine();
	            
	            if (statusLine.getStatusCode() >= 200&&statusLine.getStatusCode() < 300) {
	            	result.code =statusLine.getStatusCode();
	            	result.result= EntityUtils.toString(httpResponse.getEntity());
	            } else {
	            	result.code =statusLine.getStatusCode();
	            	result.errorType=UDeskError.ERROR_TYPE_SERVER;
	            	result.error = EntityUtils.toString(httpResponse.getEntity());//statusLine.getReasonPhrase();
	            }
			} catch ( Exception e) {
				e.printStackTrace();
            	result.error =e.getMessage();
            	result.errorType=UDeskError.ERROR_TYPE_NETWORK;
			} 

  

     

    }


    /**
     * get
     * @param httpUrl
     * @return
     */
    public static String get(String httpUrl) {
        DefaultHttpClient httpclient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(httpUrl);
        httpGet.addHeader("charset", HTTP.UTF_8);

        HttpParams params = httpclient.getParams(); // 计算网络超时用
        HttpConnectionParams.setConnectionTimeout(params, 15 * 1000);
        HttpConnectionParams.setSoTimeout(params, 10 * 1000);
        try {
            HttpResponse httpResponse = httpclient.execute(httpGet);
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                return EntityUtils.toString(httpResponse.getEntity(), HTTP.UTF_8);
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


	public static boolean isNetworkConnected(Context context) {
		if (context != null) {
			ConnectivityManager mConnectivityManager = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetworkInfo = mConnectivityManager
					.getActiveNetworkInfo();
			if (mNetworkInfo != null) {
				return mNetworkInfo.isAvailable();
			}
		}
		return false;
	}

}
