/**
 * 登录
 *
 * @author xutao
 */
package cn.udesk.saas.sdk.chat;

import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import cn.udesk.saas.sdk.OnUserInfoCallback;
import cn.udesk.saas.sdk.ResourcesIDLoader;
import cn.udesk.saas.sdk.common.HttpResult;
import cn.udesk.saas.sdk.manager.UDUserManager;
import cn.udesk.saas.sdk.utils.UDEnvConstants;
import cn.udesk.saas.sdk.utils.UDHttpUtil;
import cn.udesk.saas.sdk.utils.UDUtils;

public class UDUserInfoTask extends AsyncTask<String, Integer, HttpResult> {

    private    Map<String,String> info;
    private Map<String,String> infoExtra;
	private OnUserInfoCallback mCallback = null;
	private Context mContext;
	

    public UDUserInfoTask( Context context,OnUserInfoCallback  callback ) {
        this.mCallback = callback;
        mContext = context;
    }
    
    public void setUserInfo(Map<String,String> info ,Map<String,String> infoExtra){
    	this.info = info;
    	this.infoExtra = infoExtra;
    }
    
    private JSONObject generateHttpContent(String uid,String nickName){
		JSONObject rootjson = new JSONObject();
		
        if(UDEnvConstants.isDebugMode) {
            Log.e(TAG, "   generateHttpContetn   info:"+info+"   infoExtra:"+infoExtra);
        }
        
		try {
			// add user info
			if(info!=null && !info.isEmpty()){
				JSONObject infoJson = new JSONObject();
				
				Map<String,String> infoTemp = info;
				Set<String> keySet = infoTemp.keySet();
				for (String key : keySet) {
					infoJson.put(key, infoTemp.get(key));
				}
				rootjson.put("user_info", infoJson);
			}
			
			// add extra user info
			if(infoExtra!=null && !infoExtra.isEmpty()){
				JSONObject infoJson = new JSONObject();
 
				Map<String,String> infoTemp = infoExtra;
				Set<String> keySet = infoTemp.keySet();
				for (String key : keySet) {
					infoJson.put(key, infoTemp.get(key));
				}
				rootjson.put("extra_user_info", infoJson);
			}
			
			
			rootjson.put("user_id", uid);
			rootjson.put("nick_name", nickName);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
 
 
        if(UDEnvConstants.isDebugMode) {
            Log.e(TAG, "   generateHttpContetn   rootjson:"+rootjson.toString());
        }
    	return rootjson;
    }

    @Override
    protected HttpResult doInBackground(String... params) {
        String subDomain = params[0];
        String secretKey = params[1];
        String uid = params[2];
        String nickName = params[3];
        String sign =UDUtils.generateSign(  secretKey, uid, null);//post请求没有 key=value&key1=value1


        StringBuilder builder = new StringBuilder();
        builder.append("sign=").append(sign);

        String url = String.format("http://%s/api/v1/sdk_users.json?", UDUserManager.getInstance().getSubDomain());
        url = url + builder.toString();

        if(UDEnvConstants.isDebugMode) {
            Log.e(TAG, " request  url:"+url);
        }

        // do work
        HttpResult httpResult = new HttpResult();
        UDHttpUtil.post(url, generateHttpContent(uid,nickName).toString(),httpResult );
        
        handleHttpResult(httpResult);
        return httpResult;
    }

    private void handleHttpResult(HttpResult result){
        if(UDEnvConstants.isDebugMode) {
            Log.e(TAG, "result = " + result);
        }
        
    /*   
     * 
     *  {
       message = success;
       status = 200;
     }

     *  
     *  #status:
            #0: 成功
            #1: 服务器内部错误
            #2: 参数错误
            #1001:指定的id不存在
            #2001:二级域名错误
            #2002:sign错误 */
        if(result.code==200) { 
            try {
              JSONObject resultJson = new JSONObject(result.result);
              int status = resultJson.getInt("status");
              if(status>= 200&& status < 300){
          		if(mCallback!=null){
        			mCallback.onSuccess();
        		} 
              }else{
            		if(mCallback!=null){
            			mCallback.onFail ( resultJson.optString("message"));
            		}  
              }


            } catch(Exception e) {
        		if(mCallback!=null){
        			ResourcesIDLoader loader=ResourcesIDLoader.getResIdLoaderInstance(mContext);
        			mCallback.onFail( mContext.getString( loader.getResStringID( "udesk_error_network_parse")));
        		}  
            }
        }else{
    		if(mCallback!=null){
    			mCallback.onFail(  result.error);
    		}
        }
    }
    
    private final static String TAG = UDUserInfoTask.class.getSimpleName();
    
}
