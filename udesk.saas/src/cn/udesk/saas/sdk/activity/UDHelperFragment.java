/**
 * 知识点
 * @author xutao
 */
package cn.udesk.saas.sdk.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import cn.udesk.saas.sdk.UDeskSDK;
import cn.udesk.saas.sdk.adapter.UDHelperAdapter;
import cn.udesk.saas.sdk.helper.UDDoHelperSearchTask;
import cn.udesk.saas.sdk.helper.UDDoHelperSearchTask.OnDoHelperSearchListener;
import cn.udesk.saas.sdk.helper.UDGetHelperContentTask;
import cn.udesk.saas.sdk.helper.UDGetHelperContentTask.OnGetHelperContentListener;
import cn.udesk.saas.sdk.helper.UDGetHelperListTask;
import cn.udesk.saas.sdk.helper.UDGetHelperListTask.OnGetHelperListListener;
import cn.udesk.saas.sdk.helper.UDHelperItem;
import cn.udesk.saas.sdk.manager.UDUserManager;
import cn.udesk.saas.sdk.utils.UDEnvConstants;

public class UDHelperFragment extends UDIMFragment implements OnClickListener {


	private TextView tvNaviLeft, tvNaviTitle, tvNaviRight;
	private View naviToIm;
	private View naviFail;
    private ListView lvHelper;
    private TextView tvHelperSubject, tvHelperContent;
    private LinearLayout llHelperLoading, llHelperSearch;
    private ScrollView svHelperArticle;
    private Button btnSearch;
    private EditText etHelperSearch;

    private UDHelperAdapter mHelperAdapter = null, mSearchAdapter = null;
    private UDGetHelperListTask mGetHelperListTask = null;
    private UDGetHelperContentTask mGetHelperContentTask = null;
    private UDDoHelperSearchTask mDoHelperSearchTask = null;


    /**
     * 界面状态
     */
    private final static int STATUS_HELP_LIST      = 1;
    private final static int STATUS_SEARCH_LIST    = 2;
    private final static int STATUS_HELP_CONTENT   = 3;

    private ArrayList<Integer> statusStack = new ArrayList<Integer>(32);


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(mResIDLoader.getResLayoutID("udesk_activity_helper"), container, false);
    }

    public void onViewCreated(View rootView, Bundle savedInstanceState) {
        // ---------------------------------------------------------
        tvNaviLeft = (TextView)rootView.findViewById(mResIDLoader.getResIdID("udesk_navi_left"));
        tvNaviLeft.setText(  mResIDLoader.getResStringID( "udesk_navi_close"));
        tvNaviLeft.setOnClickListener(this);

        tvNaviTitle = (TextView)rootView.findViewById(mResIDLoader.getResIdID("udesk_navi_title"));

        tvNaviRight = (TextView)rootView.findViewById(mResIDLoader.getResIdID("udesk_navi_right"));
        naviToIm = rootView.findViewById(mResIDLoader.getResIdID("udesk_navi_to_im"));
        naviToIm.setOnClickListener(this);
        
        naviFail = rootView.findViewById(mResIDLoader.getResIdID("udesk_navi_may_search_fail"));
        
        if((UDUserManager.getInstance().getSDKMode() & UDeskSDK.MODE_IM) == UDeskSDK.MODE_IM) {
            tvNaviRight.setText(mResIDLoader.getResStringID( "udesk_navi_open_im"));
            tvNaviRight.setOnClickListener(this);
        }

        // ---------------------------------------------------------
        llHelperSearch = (LinearLayout)rootView.findViewById(mResIDLoader.getResIdID("udesk_helper_search"));
        etHelperSearch = (EditText)rootView.findViewById(mResIDLoader.getResIdID("udesk_helper_search_input"));
        etHelperSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.toString().length() == 0) {
                    if(getStatusStackTop() == STATUS_SEARCH_LIST) {
                        popStatusStack();
                    }
                }
            }
        });
        btnSearch = (Button)rootView.findViewById(mResIDLoader.getResIdID("udesk_helper_search_button"));
        btnSearch.setOnClickListener(this);
        mSearchAdapter = new UDHelperAdapter(this.getActivity());

        // ---------------------------------------------------------
        lvHelper = (ListView)rootView.findViewById(mResIDLoader.getResIdID("udesk_helper_list"));
        mHelperAdapter = new UDHelperAdapter(this.getActivity());
        lvHelper.setAdapter(mHelperAdapter);
        lvHelper.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(getStatusStackTop() == STATUS_HELP_LIST) {
                    startGetHelperContentTask(mHelperAdapter.getItem((int)id).id);
                } else {
                    startGetHelperContentTask(mSearchAdapter.getItem((int)id).id);
                }
                naviFail.setVisibility(View.GONE);
            }
        });

        // ---------------------------------------------------------
        svHelperArticle = (ScrollView)rootView.findViewById(mResIDLoader.getResIdID("udesk_helper_article"));
        tvHelperSubject = (TextView)rootView.findViewById(mResIDLoader.getResIdID("udesk_helper_subject"));
        tvHelperContent = (TextView)rootView.findViewById(mResIDLoader.getResIdID("udesk_helper_content"));


        llHelperLoading = (LinearLayout)rootView.findViewById(mResIDLoader.getResIdID("udesk_helper_loading"));

        // ---------------------------------------------------------
        startGetHelperList();

    }


    public void onDestroyView() {
        super.onDestroyView();
        cancelGetHelperListTask();
        cancelGetHelperContentTask();
        cancelDoHelperSearchTask();
    }


    @Override
    public void onClick(View v) {
        if(v.getId() == mResIDLoader.getResIdID("udesk_navi_left")) {
            // 如果是展示子目录，则回到上一级
            popStatusStack();
        } else if(v.getId() == mResIDLoader.getResIdID("udesk_navi_right")) {
            startActivity(new Intent(this.getActivity(), UDIMActivity.class));
        } else if(v.getId() == mResIDLoader.getResIdID("udesk_helper_search_button")) {
            String search = etHelperSearch.getText().toString();
            if(!TextUtils.isEmpty(search)) {
                startDoHelperSearchTask(search);
            }
        }else if(naviToIm==v){
        	 startActivity(new Intent(this.getActivity(), UDIMActivity.class));
        }
    }



    @Override
	public boolean onBackPressed() {
        popStatusStack();
        return true;
	}

    /**
     * 获取栈顶的状态值
     * @return
     */
    private int getStatusStackTop() {
        if(statusStack.size() == 0) {
            return 0;
        } else {
            return statusStack.get(statusStack.size() - 1);
        }
    }

    /**
     * 退栈
     */
    private void popStatusStack() {
        if(UDEnvConstants.isDebugMode) {
            Log.e("UDHelperActivity", "statusStack.size()=" + statusStack.size());
        }
        final Activity activity=  this.getActivity();
        if(activity==null){
            return;
        }

        if(statusStack.size() == 1) {
            activity.finish();
        } else if(statusStack.size() > 1) {

            if(llHelperLoading.isShown()) {
                llHelperLoading.setVisibility(View.GONE);
            }

            int preStatus = statusStack.get(statusStack.size() - 2);

            switch (preStatus) {
            case STATUS_HELP_LIST:
                tvNaviLeft.setText(mResIDLoader.getResStringID( "udesk_navi_close"));
                showHelperList();
                break;
            case STATUS_SEARCH_LIST:
                tvNaviLeft.setText(mResIDLoader.getResStringID( "udesk_navi_back"));
                showSearchList();
                break;
            }

            statusStack.remove(statusStack.size() - 1);
        }
    }


    /**
     * 显示loading
     */
    private void showLoading(int nextStatus) {
        llHelperSearch.setVisibility(View.GONE);
        svHelperArticle.setVisibility(View.GONE);
        lvHelper.setVisibility(View.GONE);

        llHelperLoading.setVisibility(View.VISIBLE);
        if(nextStatus == STATUS_HELP_LIST) {
            tvNaviLeft.setText(mResIDLoader.getResStringID( "udesk_navi_close"));
        } else {
            tvNaviLeft.setText(mResIDLoader.getResStringID( "udesk_navi_back"));
        }

        // 如果栈顶是同类型的界面，则不再增加新的界面
        if(nextStatus != getStatusStackTop()) {
            statusStack.add(nextStatus);
        }
    }

    /**
     * 显示问题查找后的列表
     */
    private void showSearchList() {
        tvNaviTitle.setText(mResIDLoader.getResStringID( "udesk_navi_helper_title_search"));

        svHelperArticle.setVisibility(View.GONE);
        llHelperLoading.setVisibility(View.GONE);

        llHelperSearch.setVisibility(View.VISIBLE);
        lvHelper.setAdapter(mSearchAdapter);
        lvHelper.setVisibility(View.VISIBLE);
    }

    /**
     * 显示问题列表
     */
    private void showHelperList() {
        tvNaviTitle.setText(mResIDLoader.getResStringID( "udesk_navi_helper_title_main"));
        svHelperArticle.setVisibility(View.GONE);
        llHelperLoading.setVisibility(View.GONE);

        llHelperSearch.setVisibility(View.VISIBLE);
        lvHelper.setAdapter(mHelperAdapter);
        lvHelper.setVisibility(View.VISIBLE);
        naviFail.setVisibility(View.VISIBLE);
    }

    /**
     * 显示问题内容
     */
    private void showHelperContent() {
        tvNaviTitle.setText(mResIDLoader.getResStringID( "udesk_navi_helper_title_content"));

        lvHelper.setVisibility(View.GONE);
        llHelperSearch.setVisibility(View.GONE);
        llHelperLoading.setVisibility(View.GONE);

        svHelperArticle.setVisibility(View.VISIBLE);
        
    }


    /**
     * 获取帮助列表
     */
    private void startGetHelperList() {

        showLoading(STATUS_HELP_LIST);

        cancelGetHelperListTask();

        mGetHelperListTask = new UDGetHelperListTask(new OnGetHelperListListener() {

            @Override
            public void onSucess(UDHelperItem[] items) {
                if(items.length == 0) {
                    Toast.makeText(UDHelperFragment.this.getActivity(),  mResIDLoader.getResStringID( "udesk_helper_search_empty"), Toast.LENGTH_SHORT).show();
                }
                mHelperAdapter.setList(items);
                showHelperList();
            }

            @Override
            public void onFail(String result) {
                result = (TextUtils.isEmpty(result)) ? getString(mResIDLoader.getResStringID( "udesk_helper_get_list_failed")) : result;

                Toast.makeText(UDHelperFragment.this.getActivity(), result, Toast.LENGTH_SHORT).show();

                popStatusStack();
            }
        });

        mGetHelperListTask.execute(
                UDUserManager.getInstance().getSubDomain(),
                UDUserManager.getInstance().getSecretKey());

    }


    /**
     * 取消正在执行的任务
     */
    private void cancelGetHelperListTask() {
        if(mGetHelperListTask != null && mGetHelperListTask.getStatus() == AsyncTask.Status.RUNNING) {
            mGetHelperListTask.cancelled();
            mGetHelperListTask = null;
        }
    }


    /**
     * 获取帮助内容
     */
    private void startGetHelperContentTask(int id) {

        showLoading(STATUS_HELP_CONTENT);

        cancelGetHelperContentTask();

        mGetHelperContentTask = new UDGetHelperContentTask(new OnGetHelperContentListener() {

            @Override
            public void onSucess(String subject, String content) {

                if(UDEnvConstants.isDebugMode) {
                    Log.e("UDHelperActivity", "subject : " + subject + "\ncontent : " + content);
                }

                tvHelperSubject.setText(TextUtils.isEmpty(subject) ? "" : subject);
                tvHelperContent.setText(TextUtils.isEmpty(content) ? "" : content);

                showHelperContent();
            }

            @Override
            public void onFail(String result) {
                popStatusStack();

                result = (TextUtils.isEmpty(result)) ? getString(mResIDLoader.getResStringID( "udesk_helper_get_article_failed")) : result;

                Toast.makeText(UDHelperFragment.this.getActivity(), result, Toast.LENGTH_SHORT).show();
            }

        });

        mGetHelperContentTask.execute(
                UDUserManager.getInstance().getSubDomain(),
                UDUserManager.getInstance().getSecretKey(),
                String.valueOf(id));
    }


    /**
     * 取消正在执行的任务
     */
    private void cancelGetHelperContentTask() {
        if(mGetHelperContentTask != null && mGetHelperContentTask.getStatus() == AsyncTask.Status.RUNNING) {
            mGetHelperContentTask.cancelled();
            mGetHelperContentTask = null;
        }
    }


    /**
     * 获取帮助内容
     */
    private void startDoHelperSearchTask(String query) {

        showLoading(STATUS_SEARCH_LIST);

        cancelDoHelperSearchTask();

        mDoHelperSearchTask = new UDDoHelperSearchTask(new OnDoHelperSearchListener() {

            @Override
            public void onSucess(UDHelperItem[] items) {
                if(items == null || items.length == 0) {
                    Toast.makeText(UDHelperFragment.this.getActivity(), mResIDLoader.getResStringID( "udesk_helper_search_empty"), Toast.LENGTH_SHORT).show();
                    popStatusStack();
                } else {
                    mSearchAdapter.setList(items);
                    showSearchList();
                }
            }

            @Override
            public void onFail(String result) {
                popStatusStack();

                result = (TextUtils.isEmpty(result)) ? getString(mResIDLoader.getResStringID( "udesk_helper_get_list_failed")) : result;

                Toast.makeText(UDHelperFragment.this.getActivity(), result, Toast.LENGTH_SHORT).show();
            }
        });

        mDoHelperSearchTask.execute(
                UDUserManager.getInstance().getSubDomain(),
                UDUserManager.getInstance().getSecretKey(),
                query);

    }


    private void cancelDoHelperSearchTask() {
        if(mDoHelperSearchTask != null && mDoHelperSearchTask.getStatus() == AsyncTask.Status.RUNNING) {
            mDoHelperSearchTask.cancelled();
            mDoHelperSearchTask = null;
        }
    }



}
