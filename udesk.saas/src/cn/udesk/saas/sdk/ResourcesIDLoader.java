package cn.udesk.saas.sdk;

import android.content.Context;

 public abstract class ResourcesIDLoader {
    private static ResourcesIDLoader sInstance;
    static {
        Class subClass = null;

        // 目前我们只有这两种实现类，一种Android内置的，一种是ApiCloud. aa bb这种命名主要是为了避免反编译，假冒的混淆设置
        //cn.udesk.saas.sdk.aa要放在前面，这是Android方式，调试过程中Android资源方式是优先的
        try {
            subClass = Class.forName("cn.udesk.saas.sdk.aa");//for Android 
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        if (subClass == null) {
            try {
                subClass = Class .forName("cn.udesk.saas.sdk.bb");//for apicloud
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        try {
            sInstance = (ResourcesIDLoader) subClass.newInstance();
        } catch ( Exception e) {
            //不太可能发生
            throw new RuntimeException(" fail to load resource load  ");
        }
    }

    public static ResourcesIDLoader getResIdLoaderInstance(Context context) {
        synchronized (sInstance) {
            if (!sInit ) {
                sInstance.init(context.getApplicationContext());
            }
            sInit =true;
        }
        return sInstance;
    }

    public void init(Context app) {
    };

    public abstract int getResIdID(String resName);

    public abstract int getResStringID(String resName);

    public abstract int getResLayoutID(String resName);
    public abstract int  getResDrawableID(String resName);
    
    public abstract int getResDimenID(String resName) ;

    private static boolean sInit = false;
}
