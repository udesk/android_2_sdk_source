/**
 * 对外的接口
 * 采用单例模式，一般不修改
 *
 * @author xutao
 */

package cn.udesk.saas.sdk;

import java.util.HashMap;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import cn.udesk.saas.sdk.activity.UDHelperActivity;
import cn.udesk.saas.sdk.activity.UDIMActivity;
import cn.udesk.saas.sdk.manager.UDUserManager;
import cn.udesk.saas.sdk.utils.CommonUtil;
import cn.udesk.saas.sdk.utils.DialogUtil;
import cn.udesk.saas.sdk.utils.UDEnvConstants;

public class UDeskSDK {

    public final static int MODE_HELPER   = 0x01;
    public final static int MODE_IM       = 0x02;
    public final static int MODE_BOTH     = (MODE_HELPER | MODE_IM);

    private int sdkMode = MODE_BOTH;

    /**
     * 接口是单例形式的
     */
    private static UDeskSDK instance = null;

    private UDeskSDK() {
    }

    public static UDeskSDK getInstance() {
        if(instance == null) {
            instance = new UDeskSDK();
        }
        return instance;
    }

    public void setMode(int mode) {
        sdkMode = mode;
        UDUserManager.getInstance().setSDKMode(mode);
    }

    /**
     * 设置二级域名，必须在登录前调用(必接)
     * @param subDomain
     */
    @Deprecated
    public void setSubDomain(String subDomain) {
        UDUserManager.getInstance().setSubDomain(subDomain);
    }

    /**
     * 设置密钥，必须在登录前调用(必接)
     * @param secretKey
     */
    @Deprecated
    public void setSecretKey(String secretKey) {
        UDUserManager.getInstance().setSecretKey(secretKey);
    }

    public void init(String secretKey,String subDomain){
        UDUserManager udm =UDUserManager.getInstance();
        udm.setSubDomain(subDomain);
        udm.setSecretKey(secretKey);
    }
    /**
     * @deprecated
     * 设置第三方应用的用户ID(选接)
     * @param appUserId 第三方应用的用户ID
     */
    public void setUserInfo(String userId, String nick) {
        UDUserManager.getInstance().setUserInfo(userId, nick);
    }
    
    public void setUserInfo(Context context, HashMap<String,String> info, OnUserInfoCallback onUserInfoCallback) {
    	setUserInfo(context,info ,null, onUserInfoCallback);
    }
    
    public void setUserInfo(Context context,String userId, String nick, HashMap<String,String> info, OnUserInfoCallback onUserInfoCallback) {
    	if(  !TextUtils.isEmpty(userId) && !TextUtils.isEmpty(nick)){
    		setUserInfo(userId,nick);
    	}
    	setUserInfo(context,info ,null, onUserInfoCallback);
    }


    //TODO 后续版本要开发出来给用户添加 额外信息
    private /*public*/ void setUserInfo(Context context, HashMap<String,String> info,HashMap<String,String> infoExtra,OnUserInfoCallback onUserInfoCallback) {
        UDUserManager.getInstance().setUserInfo(  context,    info,  infoExtra,  onUserInfoCallback);
    }
    /**
     * 打开对应的activity
     * @param activity
     */
    public void open(Context context) {

        if(UDEnvConstants.isDebugMode) {
            Log.e("udesksdk", "sdkMode = " +sdkMode);
        }

        Intent intent;
        if((sdkMode & MODE_HELPER) == MODE_HELPER) {
            if(UDEnvConstants.isDebugMode) {
                Log.e("udesksdk", "MODE_HELPER");
            }
            intent = new Intent(context, UDHelperActivity.class);
        } else {
            intent = new Intent(context, UDIMActivity.class);
        }
        context.startActivity(intent);
    }
    
    
	public void showFAQs( final Context context ){
		setMode(MODE_BOTH);
		open(context);
	}
	
	public void showConversation( final Context context ){
		showConversation(context,true,true);
	}
	
	public void showConversation( final Context context,final boolean isShowPictureFunction,final boolean isShowEmotionFunction ){
		setMode(MODE_IM);
		
		Intent intent = new Intent(context, UDIMActivity.class);
		intent.putExtra("showPicture", isShowPictureFunction);
		intent.putExtra("showEmotion", isShowEmotionFunction);
        context.startActivity(intent);
	}
	
	public void showFAQSection(final Context context  ){
		setMode(MODE_HELPER);
		open(context);
	}
	

 
/**
 * 启动一个应用评分的界面。<br>
 * UI布局文件请参考udesk_layout_rate_dialog.xml<br>
 * @param context
 * @param isShowPictureFunction
 * @param isShowEmotionFunction
 */
	public void showRateDialog(Context context, final boolean isShowPictureFunction,final boolean isShowEmotionFunction){
		DialogUtil.showRateDialog(context,isShowPictureFunction,isShowEmotionFunction);
	}
	
	/**
	 * 
	 * @param context 请确保这个context为一个 activity或者Application
	 */
	public   void startRateToAppStore(Context context ) {
		CommonUtil.startRateToAppStore(context, context.getApplicationInfo().packageName);
	}
	

}
