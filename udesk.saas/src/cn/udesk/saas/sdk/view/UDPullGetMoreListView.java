/**
 * 自定义下啦刷新，用来拉取本地历史记录
 * @author xutao
 */
package cn.udesk.saas.sdk.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import cn.udesk.saas.sdk.ResourcesIDLoader;
import cn.udesk.saas.sdk.utils.UDEnvConstants;

public class UDPullGetMoreListView extends ListView implements OnScrollListener {

	private static final String TAG = "UDPullGetMoreListView";

    private final static int RELEASE_To_REFRESH = 0;
    private final static int PULL_To_REFRESH = 1;
    private final static int REFRESHING = 2;
    private final static int DONE = 3;
    private final static int LOADING = 4;

    // 实际的padding的距离与界面上偏移距离的比例
    private final static int RATIO = 3;

    private LayoutInflater inflater;

    private LinearLayout llheader;

    private TextView tvTips;
    private ProgressBar pbLoading;
    
    private  ResourcesIDLoader resIDLoader;

    // 用于保证startY的值在一个完整的touch事件中只被记录一次
    private boolean isRecored;

    private int headContentHeight;

    private int startY;
    private int firstItemIndex;
    private int state;
    private boolean isBack;
    private OnRefreshListener refreshListener;

    private boolean isRefreshable;
    private boolean isPush;


    public UDPullGetMoreListView(Context context) {
        super(context);
        init(context);
    }

    public UDPullGetMoreListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
    	resIDLoader=ResourcesIDLoader.getResIdLoaderInstance(context);
        inflater = LayoutInflater.from(context);
        llheader = (LinearLayout) inflater.inflate(resIDLoader.getResLayoutID("udesk_layout_get_more"), null);
        pbLoading = (ProgressBar) llheader.findViewById(resIDLoader.getResIdID("udesk_get_more_progress"));
        tvTips = (TextView) llheader.findViewById(resIDLoader.getResIdID("udesk_get_more_tips"));

        measureView(llheader);
        headContentHeight = llheader.getMeasuredHeight();

        llheader.setPadding(0, -1 * headContentHeight, 0, 0);
        llheader.invalidate();

        addHeaderView(llheader, null, false);

        setOnScrollListener(this);

        state = DONE;
        isRefreshable = false;
        isPush = true;
    }


    public void onScroll(AbsListView arg0, int firstVisiableItem, int arg2, int arg3) {
        firstItemIndex = firstVisiableItem;
        if(firstItemIndex == 1 && !isPush) {
            setSelection(0);
        }
    }


    public void onScrollStateChanged(AbsListView arg0, int arg1) {
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (isRefreshable) {
            switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (firstItemIndex == 0 && !isRecored) {
                    isRecored = true;
                    isPush = true;
                    startY = (int) event.getY();
                    if(UDEnvConstants.isDebugMode) Log.w(TAG, "在down时候记录当前位置‘");
                }
                break;
            case MotionEvent.ACTION_UP:
                if (state != REFRESHING && state != LOADING) {
                    if (state == DONE) {
                        // 什么都不做
                    }
                    if (state == PULL_To_REFRESH) {
                        state = DONE;
                        changeHeaderViewByState();

                        if(UDEnvConstants.isDebugMode) Log.w(TAG, "由下拉刷新状态，到done状态");
                    }
                    if (state == RELEASE_To_REFRESH) {
                        state = REFRESHING;
                        changeHeaderViewByState();
                        onRefresh();

                        if(UDEnvConstants.isDebugMode) Log.w(TAG, "由松开刷新状态，到done状态");
                    }
                }

                isRecored = false;
                isBack = false;

                break;

            case MotionEvent.ACTION_MOVE:
                int tempY = (int) event.getY();

                if (!isRecored && firstItemIndex == 0) {
                    if(UDEnvConstants.isDebugMode) Log.w(TAG, "在move时候记录下位置");
                    isRecored = true;
                    startY = tempY;
                }

                if (state != REFRESHING && isRecored && state != LOADING) {

                    // 保证在设置padding的过程中，当前的位置一直是在head，否则如果当列表超出屏幕的话，当在上推的时候，列表会同时进行滚动

                    // 可以松手去刷新了
                    if (state == RELEASE_To_REFRESH) {

                        setSelection(0);

                        // 往上推了，推到了屏幕足够掩盖head的程度，但是还没有推到全部掩盖的地步
                        if (((tempY - startY) / RATIO < headContentHeight) && (tempY - startY) > 0) {
                            state = PULL_To_REFRESH;
                            changeHeaderViewByState();

                            if(UDEnvConstants.isDebugMode) Log.w(TAG, "由松开刷新状态转变到下拉刷新状态");
                        }
                        // 一下子推到顶了
                        else if (tempY - startY <= 0) {
                            state = DONE;
                            changeHeaderViewByState();

                            if(UDEnvConstants.isDebugMode) Log.w(TAG, "由松开刷新状态转变到done状态");
                        }
                        // 往下拉了，或者还没有上推到屏幕顶部掩盖head的地步
                        else {
                            // 不用进行特别的操作，只用更新paddingTop的值就行了
                        }
                    }
                    // 还没有到达显示松开刷新的时候,DONE或者是PULL_To_REFRESH状态
                    if (state == PULL_To_REFRESH) {

                        setSelection(0);

                        // 下拉到可以进入RELEASE_TO_REFRESH的状态
                        if ((tempY - startY) / RATIO >= headContentHeight) {
                            state = RELEASE_To_REFRESH;
                            isBack = true;
                            changeHeaderViewByState();
                            if(UDEnvConstants.isDebugMode) Log.w(TAG, "由done或者下拉刷新状态转变到松开刷新");
                        }
                        // 上推到顶了
                        else if (tempY - startY <= 0) {
                            state = DONE;
                            changeHeaderViewByState();
                            isPush = false;
                            if(UDEnvConstants.isDebugMode) Log.w(TAG, "由DOne或者下拉刷新状态转变到done状态");
                        }
                    }

                    // done状态下
                    if (state == DONE) {
                        if (tempY - startY > 0) {
                            state = PULL_To_REFRESH;
                            changeHeaderViewByState();
                        }
                    }

                    // 更新headView的size
                    if (state == PULL_To_REFRESH) {
                        llheader.setPadding(0, -1 * headContentHeight + (tempY - startY) / RATIO, 0, 0);

                    }

                    // 更新headView的paddingTop
                    if (state == RELEASE_To_REFRESH) {
                        llheader.setPadding(0, (tempY - startY) / RATIO - headContentHeight, 0, 0);
                    }

                }

                break;
            }
        }

        return super.onTouchEvent(event);
    }

    // 当状态改变时候，调用该方法，以更新界面
    private void changeHeaderViewByState() {
        switch (state) {
        case RELEASE_To_REFRESH:
            pbLoading.setVisibility(View.GONE);
            tvTips.setVisibility(View.VISIBLE);
            tvTips.setText(getResources().getString(resIDLoader.getResStringID( "udesk_release_to_get_more")));

            if(UDEnvConstants.isDebugMode) Log.w(TAG, "当前状态，松开刷新");
            break;

        case PULL_To_REFRESH:
            pbLoading.setVisibility(View.GONE);
            tvTips.setVisibility(View.VISIBLE);
            // 是由RELEASE_To_REFRESH状态转变来的
            if (isBack) {
                isBack = false;
            }
            tvTips.setText(getResources().getString(resIDLoader.getResStringID( "udesk_get_more_history")));
            if(UDEnvConstants.isDebugMode) Log.w(TAG, "当前状态，下拉刷新");
            break;

        case REFRESHING:

            pbLoading.setVisibility(View.VISIBLE);
            tvTips.setText(getResources().getString(resIDLoader.getResStringID( "udesk_loading_more")));
            llheader.setPadding(0, 0, 0, 0);

            if(UDEnvConstants.isDebugMode) Log.w(TAG, "当前状态,正在刷新...");
            break;

        case DONE:

            pbLoading.setVisibility(View.GONE);
            tvTips.setText(getResources().getString(resIDLoader.getResStringID( "udesk_get_more_history")));
            llheader.setPadding(0, -1 * headContentHeight, 0, 0);

            if(UDEnvConstants.isDebugMode) Log.w(TAG, "当前状态，done");
            break;
        }
    }

    public void setOnRefreshListener(OnRefreshListener refreshListener) {
        this.refreshListener = refreshListener;
        isRefreshable = true;
    }


    public interface OnRefreshListener {
        public void onRefresh();
    }


    public void onRefreshComplete() {
        state = DONE;
        changeHeaderViewByState();
        invalidateViews();
    }


    private void onRefresh() {
        if (refreshListener != null) {
            refreshListener.onRefresh();
        }
    }


    public void clickToRefresh() {
        state = REFRESHING;
        changeHeaderViewByState();
    }


    private void measureView(View child) {
        ViewGroup.LayoutParams p = child.getLayoutParams();
        if (p == null) {
            p = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        int childWidthSpec = ViewGroup.getChildMeasureSpec(0, 0 + 0, p.width);
        int lpHeight = p.height;
        int childHeightSpec;
        if (lpHeight > 0) {
            childHeightSpec = MeasureSpec.makeMeasureSpec(lpHeight, MeasureSpec.EXACTLY);
        } else {
            childHeightSpec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
        }
        child.measure(childWidthSpec, childHeightSpec);
    }

    @Override
	public int pointToPosition(int x, int y) {
    	mX =x;
    	mY =y;
		return super.pointToPosition(x, y);
	}
    private int mX,mY;
    
    public int[] getRealPosition(){
    	return new int[]{mX,mY};
    }

}