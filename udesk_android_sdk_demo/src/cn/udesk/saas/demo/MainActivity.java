package cn.udesk.saas.demo;


import java.util.HashMap;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import cn.udesk.saas.sdk.OnUserInfoCallback;
import cn.udesk.saas.sdk.UDeskSDK;
import cn.udesk.saas.sdk.UDeskUserInfo;

public class MainActivity extends Activity {

    private final static String UDESK_SUB_DOMAIN = "在udesk平台填写的域名";
    private final static String UDESK_SECRET_KEY = "udesk平台分配的secret key";

    private EditText etSubDomain, etSecret;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 设置用户信息[可选接口]
       UDeskSDK.getInstance().setUserInfo("1222", "nick_nick1");

        etSubDomain = (EditText)findViewById(R.id.et_subdomain);
        etSecret = (EditText)findViewById(R.id.et_secret);
           
         //etSubDomain.setText("udesk9527.udesk.cn");
         //etSecret.setText("8cacea21b1519089830e8e7d360471d7");
        
       // etSubDomain.setText("reocar.tiyanudesk.com");
       // etSecret.setText("b7cfcf9924570a54f73fe8728bf350c4");
        
        etSubDomain.setText("shushu.udesk.cn");
        etSecret.setText("6ff1de345a07d360bb9d50d262d8eaeb");

        findViewById(R.id.btn_open_helper).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // 设置当前模式
                //openUdesk(UDeskSDK.MODE_HELPER);
            	UDeskSDK.getInstance().showFAQSection(MainActivity.this);
            }
        });

        findViewById(R.id.btn_open_im).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //openUdesk(UDeskSDK.MODE_IM);
            	setDomainKey();
            	UDeskSDK.getInstance().showConversation(MainActivity.this, true,true);
            }
        });

        findViewById(R.id.btn_open_all).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // 打开UDesk
               // openUdesk(UDeskSDK.MODE_BOTH);
            	UDeskSDK.getInstance().showFAQs(MainActivity.this);
            }
        });
        
        testAddUserInfo();//请在初始化时 进行信息的添加
        
        
        findViewById(R.id.btn_grade_feedback).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
            	UDeskSDK.getInstance().showRateDialog(MainActivity.this, true,false);//测试代码
            }
        });
    }

    private void testAddUserInfo(){
    	setDomainKey();
    	
        HashMap<String,String> info = new HashMap<String,String>();
        info.put(UDeskUserInfo.NICK_NAME, "udesk");
        info.put(UDeskUserInfo.CELLPHONE, "010-63369163");
        info.put(UDeskUserInfo.DESCRIPTION, " Desc desc ");
        info.put(UDeskUserInfo.EMAIL, "support12345@udesk.cn");
        info.put(UDeskUserInfo.QQ, "123456");
        info.put(UDeskUserInfo.WEIBO_NAME, "weiboname");
        info.put(UDeskUserInfo.WEIXIN_ID, "weixin");
        UDeskSDK.getInstance().setUserInfo(this, info,  new OnUserInfoCallback() {
        	public void onSuccess(){
        		
        	}

        	public void onFail(String message){
        		
        	}
		});
    }

    private void setDomainKey(){
        String subdomain = etSubDomain.getText().toString();
        if(TextUtils.isEmpty(subdomain)) {
            subdomain = UDESK_SUB_DOMAIN;
        }

        String secret = etSecret.getText().toString();
        if(TextUtils.isEmpty(secret)) {
            secret = UDESK_SECRET_KEY;
        }

        // 设置UDesk平台分配的二级域名及secret key
        UDeskSDK.getInstance().init( secret,subdomain);
    }
    
    /*private void openUdesk(int mode) {
    	setDomainKey();

        // 设置当前模式
        UDeskSDK.getInstance().setMode(mode);

        // 打开UDesk
        UDeskSDK.getInstance().open(MainActivity.this);

    }*/


}
