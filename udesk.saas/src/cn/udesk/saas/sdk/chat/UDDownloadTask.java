/**
 * 下载图片任务
 * @author xutao
 */
package cn.udesk.saas.sdk.chat;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import cn.udesk.saas.sdk.utils.UDEnvConstants;
import cn.udesk.saas.sdk.utils.UDUtils;


public class UDDownloadTask extends AsyncTask<String, Integer, String> {

    private final static String TAG = "DownloadTask";

    private Context      mContext;
    private Handler      mHandler;
    private String       downloadUrl;
    private boolean      cancelled = false;
    private int thumbnailMaxWH;

    public UDDownloadTask(Context context, String url, Handler handler) {
        mContext = context;
        mHandler = handler;
        downloadUrl = url;
        cancelled = false;
        thumbnailMaxWH = (int)(context.getResources().getDisplayMetrics().density * 100);
    }


    @Override
    protected String doInBackground(String... params) {

        if(TextUtils.isEmpty(downloadUrl)) {
            return null;
        }

        String imageName = UDUtils.MD5(downloadUrl);

        File downloadFile = UDUtils.getOutputMediaFile(imageName + "_download.jpg");
        File thumbnailFile = new File(downloadFile.getPath().replace("_download", "_thumbnail"));
        if(UDEnvConstants.isDebugMode) {
            Log.w(TAG, "downloadFile = " + downloadFile.getPath());
        }

        if(thumbnailFile.exists()) {
            sendSuccessMessage(thumbnailFile.getPath());
            return null;
        }

        String tempFilePath = downloadFile.getPath() + ".tmp";
        File tempFile = new File(tempFilePath);
        if(UDEnvConstants.isDebugMode) {
            Log.w(TAG, "tempFilePath = " + tempFilePath);
        }

        // 最多重试5次
        int MAX_RETRY_TIMES = 5;
        int currentRetryTimes = 0;

        try {
            while (!cancelled && currentRetryTimes < MAX_RETRY_TIMES) {

                boolean isFileAppend = false;
                long fileLength = tempFile.length();
                final int blockSize = 1024 * 30;
                byte[] buffer = new byte[blockSize];
                int size = 0;

                // 获取实体数据的范围
                if(tempFile.exists() && fileLength > 0) {
                    isFileAppend = true;
                }

                HttpURLConnection connection = (HttpURLConnection)new URL(downloadUrl).openConnection();
                int imageSize = connection.getContentLength();

                if(imageSize > fileLength) {

                    connection.setConnectTimeout(5 * 1000);

                    // 设置断点续传数据的范围
                    if(isFileAppend) {
                        connection.setRequestProperty("Range", "bytes=" + fileLength + "-");
                    }

                    DataInputStream dis = new DataInputStream(connection.getInputStream());
                    FileOutputStream fos = new FileOutputStream(tempFile, isFileAppend);

                    while (!cancelled && ((size = dis.read(buffer, 0, blockSize)) != -1)) {
                        fos.write(buffer, 0, size);
                    }

                    dis.close();
                    dis = null;

                    fos.close();
                    fos = null;

                }

                if(imageSize == fileLength) {

                    if(!tempFile.renameTo(downloadFile)) {
                        mHandler.sendEmptyMessage(12);
                        return null;
                    }

                    BitmapFactory.Options factoryOptions = new BitmapFactory.Options();
                    factoryOptions.inJustDecodeBounds = false;
                    BitmapFactory.decodeFile(downloadFile.getPath(), factoryOptions);

                    int width = factoryOptions.outWidth;
                    int height = factoryOptions.outHeight;
                    int max = Math.max(width, height);

                    if(!thumbnailFile.exists()) {

                        factoryOptions.inJustDecodeBounds = false;
                        factoryOptions.inSampleSize = (int)Math.ceil(max / thumbnailMaxWH);
                        factoryOptions.inPurgeable = true;

                        FileOutputStream fos = new FileOutputStream(thumbnailFile);
                        Bitmap thumbnailImage = BitmapFactory.decodeFile(downloadFile.getPath(), factoryOptions);
                        thumbnailImage.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                        fos.close();
                        fos = null;

                        if(thumbnailImage != null) {
                            thumbnailImage.recycle();
                            thumbnailImage = null;
                        }
                    }


                    if(UDEnvConstants.isDebugMode) {
                        Log.w(TAG, "download Ok " + thumbnailFile.getPath());
                    }

                    sendSuccessMessage(thumbnailFile.getPath());
                    return null;

                } else {
                    currentRetryTimes += 1;
                    if(UDEnvConstants.isDebugMode) {
                        Log.w(TAG, "retry =========== " + currentRetryTimes);
                    }
                    try {
                        Thread.sleep(currentRetryTimes * currentRetryTimes * 1000);
                    } catch (Exception e) {}
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        mHandler.sendEmptyMessage(12);
        return null;
    }


    private void sendSuccessMessage(String result) {
        // 下载取消
        if(cancelled) {
            return;
        }

        if(!TextUtils.isEmpty(result)) {
            File file = new File(result);
            if(file.exists()) {
                Message msg = mHandler.obtainMessage(11);
                msg.obj = result;
                mHandler.sendMessage(msg);
                return;
            }
        }
    }


    public void cancelled() {
        cancelled = true;
    }


}
