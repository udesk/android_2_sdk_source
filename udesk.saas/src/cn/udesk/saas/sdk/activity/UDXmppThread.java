/**
 * Xmpp协议
 *
 * @author xutao
 */
package cn.udesk.saas.sdk.activity;

import java.util.Date;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.muc.DiscussionHistory;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import cn.udesk.saas.sdk.chat.UDIMMessage;
import cn.udesk.saas.sdk.db.UDIMDBManager;
import cn.udesk.saas.sdk.manager.UDUserManager;
import cn.udesk.saas.sdk.utils.UDEnvConstants;

public class UDXmppThread extends Thread implements PacketListener,ConnectionListener {

    private final static String TAG = "UDXmppThread";

//    private static final String SERVICE = "tiyanudesk.com";
//    private static final String HOST = "tiyanudesk.com";
    private static final int PORT = 5222;


    private Handler mHandler;

    private XMPPTCPConnection xmppConnection = null;
    
    MultiUserChat muc;

    private boolean isRunning = true;

    Presence mPresence = new Presence(Presence.Type.available);
    
    PacketFilter mFilter = new MessageTypeFilter(Message.Type.chat);
    
    ConnectionConfiguration mConfiguration ;
    
    DiscussionHistory mHistory  ;
    Date mNowDate=new Date();;

    /**
     * 构造
     * @param handler
     */
    public UDXmppThread(Handler handler) {
        this.mHandler = handler;
        isRunning = true;
    }

    /**
     * 断开连接
     */
    public void cancel() {
        isRunning = false;
    }


    @Override
    public void run() {
    	android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND + android.os.Process.THREAD_PRIORITY_LESS_FAVORABLE);
        
        try {
            while(isRunning) {
                if(mConfiguration==null){
               	    init();
                }
           
                if(xmppConnection == null || !xmppConnection.isConnected()) {
                    connectXMPPServer();
                    xmppConnection.addPacketListener(this, mFilter);
                    xmppConnection.addConnectionListener(this);
                    
                    if(UDEnvConstants.isDebugMode) {
                        Log.w(TAG, "xmpp connection sucess");
                    }
                }
                sleep(1000);
            }

            if(UDEnvConstants.isDebugMode) {
                Log.e(TAG, "close xmpp connection");
            }

            if(xmppConnection != null) {
                xmppConnection.disconnect();
            }
            xmppConnection = null;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 发送消息
     * @param text
     */
      boolean sendMessage(int type, String text,long rowId) {
        Message xmppMsg = new Message(
                UDUserManager.getInstance().getCustomerServiceId(),
                Message.Type.chat);

        try {
            JSONObject json = new JSONObject();
            json.put("type", type == UDIMMessage.TYPE_IMAGE ? "image" : "message");
            JSONObject data = new JSONObject();
            data.put("content", text);
            json.put("data", data);
            xmppMsg.setBody(json.toString());
        } catch (Exception e) {
        }

    	boolean success = false;
        if (xmppConnection != null) {
            try {
                xmppConnection.sendPacket(xmppMsg);
                success = true;
            } catch (Exception e) {
                e.printStackTrace();
                success = false;
            }
            
            UDIMDBManager.getInstance().markFlag(rowId,success);
        }
        
        return success;
    }
    private void init(){
    	mConfiguration = new ConnectionConfiguration(
                UDUserManager.getInstance().getDomain(), /* HOST */
                PORT,
                UDUserManager.getInstance().getDomain() /* SERVICE */);
    	mConfiguration.setSecurityMode(SecurityMode.disabled);
    	mConfiguration.setDebuggerEnabled(UDEnvConstants.isDebugMode);
 
        xmppConnection = new XMPPTCPConnection(mConfiguration);
        // 设置数据接受
        
          muc = new MultiUserChat(xmppConnection,
                UDUserManager.getInstance().getRoomId());
          mHistory = new DiscussionHistory();
    }
    
    /**
     * 连接xmpp服务器
     */
    private void connectXMPPServer() {
        try {
            // 登录
            xmppConnection.connect();
            xmppConnection.login(UDUserManager.getInstance().getLoginName(),  UDUserManager.getInstance().getLoginPassword());
            xmppConnection.sendPacket(mPresence);

            if(mHandler != null) {
                mHandler.sendEmptyMessage(0);
            }


            // 在聊天室登记
            mNowDate.setTime(System.currentTimeMillis());
            mHistory.setSince(mNowDate);
            muc.join(UDUserManager.getInstance().getLoginName(),
                    UDUserManager.getInstance().getLoginPassword(),
                    mHistory,
                    SmackConfiguration.getDefaultPacketReplyTimeout());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void processPacket(Packet packet) {
        // 是否是消息
        if(packet instanceof Message) {
            Message message = (Message) packet;
            String from = message.getFrom();//发送方
            String to = message.getTo();//接收方
            if (message.getBody() != null) {
                String fromName = StringUtils.parseBareAddress(message.getFrom());

                if(UDEnvConstants.isDebugMode) {
                    Log.i("xxx", "Text Recieved " + message.getBody() + " from " + fromName );
                }

                String body = message.getBody();

                UDIMMessage udMessage = new UDIMMessage();

                try {
                    JSONObject json = new JSONObject(body);

                    if(json.has("type")) {
                        String type = json.optString("type");
                        if(type.equalsIgnoreCase("image")) {
                            udMessage.type = UDIMMessage.TYPE_IMAGE;
                        } else {
                            udMessage.type = UDIMMessage.TYPE_TEXT;
                        }
                    }
                    if(json.has("data")) {
                        JSONObject data = json.getJSONObject("data");
                        if(data.has("content")) {
                            udMessage.text_url = data.optString("content");
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(!TextUtils.isEmpty(udMessage.text_url)) {
                    if(mHandler != null) {
                        android.os.Message msg = mHandler.obtainMessage(1);
                        msg.obj = udMessage;
                        mHandler.sendMessage(msg);
                    }
                }
            }

        } else if (packet instanceof Presence) {  // 好友状态
            Presence presence = (Presence) packet;
            String from = presence.getFrom();//发送方
            String to = presence.getTo();//接收方
            if(presence.getType().equals(Presence.Type.available)) {
                if(mHandler != null) {
                    android.os.Message msg = mHandler.obtainMessage(2);
                    msg.obj = "true";
                    mHandler.sendMessage(msg);
                }
            } else if(presence.getType().equals(Presence.Type.unavailable)) {
                if(mHandler != null) {
                    android.os.Message msg = mHandler.obtainMessage(2);
                    msg.obj = "false";
                    mHandler.sendMessage(msg);
                }
            }

        }
    }

    /**
     * 加入聊天室
     * @param user
     * @param pwd 会议室密码
     * @param roomName
     * @return
     */
    public void joinRoom(String user, String pwd, String roomName){
//        MultiUserChat muc = new MultiUserChat(con, U);
//        DiscussionHistory history = new DiscussionHistory();
//        history.setMaxStanzas(100);
//        history.setSince(new Date());
//        try {
//            muc.join(user, pwd, history, SmackConfiguration.getDefaultPacketReplyTimeout());
//        } catch (XMPPException e) {
//            return null;
//        }
//        return muc;
    }

	@Override
	public void authenticated(XMPPConnection arg0) {
	}

	@Override
	public void connected(XMPPConnection arg0) {
		
	}

	@Override
	public void connectionClosed() {
		
	}

	@Override
	public void connectionClosedOnError(Exception arg0) {
	}

	@Override
	public void reconnectingIn(int arg0) {
	}

	@Override
	public void reconnectionFailed(Exception arg0) {
	}

	@Override
	public void reconnectionSuccessful() {
	}

}
