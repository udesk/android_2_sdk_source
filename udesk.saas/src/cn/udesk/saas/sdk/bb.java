package cn.udesk.saas.sdk;

import com.uzmap.pkg.uzcore.UZResourcesIDFinder;
import cn.udesk.saas.sdk.ResourcesIDLoader;

/**
 * 目前资源差异这块，ApiCloud相关的资源加载类      20150504<br>
 * 这个类的Class Name请勿修改，不要混淆，后续要用Class.forName()来查找实现，请查阅 ResourcesIDLoader <br>
 *  * 类名称比较怪异，主要为了防止反编译<br>
 * 
 * @author lvwz
 *
 */
  class bb extends ResourcesIDLoader   {
	public int getResIdID(String resName) {
		return UZResourcesIDFinder.getResIdID(resName);
	}

	public int getResStringID(String resName) {
		return UZResourcesIDFinder.getResStringID(resName);
	}

	public int getResLayoutID(String resName) {
		return UZResourcesIDFinder.getResLayoutID(resName);
	}

	
	
	public int getResDrawableID(String resName) {
		return UZResourcesIDFinder.getResDrawableID(resName);
	}
 
	public int getResDimenID(String resName) {
        return UZResourcesIDFinder.getResDimenID(resName);
    }
}
