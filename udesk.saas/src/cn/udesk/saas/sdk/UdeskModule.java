package cn.udesk.saas.sdk;

import android.text.TextUtils;

import com.uzmap.pkg.uzcore.UZWebView;
import com.uzmap.pkg.uzcore.uzmodule.UZModule;
import com.uzmap.pkg.uzcore.uzmodule.UZModuleContext;

public class UdeskModule extends UZModule {
	public UdeskModule(UZWebView webView) {
		super(webView);
	}

	public void jsmethod_initUdesk(final UZModuleContext moduleContext){
		// setup 1: check key
		String key = moduleContext.optString("key");
		if(TextUtils.isEmpty(key)){
			moduleContext.success("请输入正确的Key值", false, true);
		}
		
		// setup 2: check domain
		String domain=moduleContext.optString("domain");
		if(TextUtils.isEmpty(domain)){//TODO 还应检查下格式
			moduleContext.success("请输入正确的domain值", false, true);
		}
		
		// setup 3: set value
		UDeskSDK sdk = UDeskSDK.getInstance();
		sdk.setSecretKey(key);
		sdk.setSubDomain(domain);
	}
	
	public void jsmethod_showFAQs(final UZModuleContext moduleContext){
		openUdesk(moduleContext,UDeskSDK.MODE_BOTH);
	}
	
	public void jsmethod_showConversation(final UZModuleContext moduleContext){
		openUdesk(moduleContext,UDeskSDK.MODE_IM);
	}
	
	public void jsmethod_showFAQSection( final UZModuleContext moduleContext){
		openUdesk(moduleContext, UDeskSDK.MODE_HELPER);
	}
	
	public void jsmethod_addUserInfo(final UZModuleContext moduleContext){
		
	}
	
	
	
	public void jsmethod_setShowPictureFunction(final UZModuleContext moduleContext){
		
	}
    private void openUdesk(final UZModuleContext moduleContext,int mode) {
        // 设置当前模式
        UDeskSDK.getInstance().setMode(mode);
        // 打开UDesk
        UDeskSDK.getInstance().open(moduleContext.getContext());

    }
	
}
