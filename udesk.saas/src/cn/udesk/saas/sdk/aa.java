package cn.udesk.saas.sdk;

import com.uzmap.pkg.uzcore.UZResourcesIDFinder;

import android.content.Context;
import android.content.res.Resources;
import cn.udesk.saas.sdk.ResourcesIDLoader;
/**
 * 目前资源差异这块，Android相关的资源加载类      20150504<br>
 * 这个类的Class Name请勿修改，不要混淆，后续在ResourcesIDLoader.java中要用Class.forName()来查找实现 <br>
 *  * 类名称比较怪异，主要为了防止反编译<br>
 * 
 * @author lvwz
 *
 */
  class aa extends ResourcesIDLoader {

	public int getResIdID(  String resName) {
		return mRes.getIdentifier(resName,"id", mPkgName);
	}

	public int getResStringID(  String resName) {
		return mRes.getIdentifier(resName, "string", mPkgName);
	}

	public int getResLayoutID(  String resName) {
		return mRes.getIdentifier(resName,   "layout", mPkgName);
	}

	public   int  getResDrawableID(String resName){
		return mRes.getIdentifier(resName, "drawable", mPkgName);
	}
	
	   public int getResDimenID(String resName) {
	       return mRes.getIdentifier(resName, "dimen", mPkgName);
	    }
	
	public void init(Context context) {
		if (mRes == null) {
			super.init(context.getApplicationContext());
			mRes = context.getApplicationContext().getResources();
			mPkgName = context.getApplicationContext().getPackageName();
		}

	};

	private Resources mRes;
	private String mPkgName;

}
