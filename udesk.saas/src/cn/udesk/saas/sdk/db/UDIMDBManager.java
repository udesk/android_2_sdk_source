/**
 * 消息历史纪录数据库
 * @author xutao
 */
package cn.udesk.saas.sdk.db;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import cn.udesk.saas.sdk.chat.UDIMMessage;
import cn.udesk.saas.sdk.utils.UDEnvConstants;

public class UDIMDBManager {

    private final static String TAG = "UDIMDBManager";

    public final static int HISTORY_COUNT = 20;

    private static UDIMDBHelper helper;
    private static UDIMDBManager instance;

    /**
     * 单例
     * @return
     */
    public static synchronized UDIMDBManager getInstance() {
        if(instance == null) {
            instance = new UDIMDBManager();
        }
        return instance;
    }


    /**
     * 初始化DBHelper，需要在使用数据库之前调用此方法
     * @param context
     */
    public synchronized void init(Context context) {
        if(helper == null) {
            helper = new UDIMDBHelper(context);
        }
    }


/**
 * 
 * @param message
 * @return 如果插入成功，返回该记录的行号。 失败则返回-1
 */
    public synchronized long  addMessageForDB(UDIMMessage message) {
       /* StringBuilder writeSqlBuilder = new StringBuilder("insert into ")
            .append(UDIMDBHelper.TABLE_NAME).append(" (")
            .append(UDIMDBHelper.COLUMN_TYPE).append(", ")
            .append(UDIMDBHelper.COLUMN_TEXT_URL).append(", ")
            .append(UDIMDBHelper.COLUMN_THUMBNAIL_PATH)
            .append(") values (")
            .append(message.type).append(", ")
            .append("\"").append(message.text_url).append("\", ")
            .append("\"").append(message.thumbnailPath).append("\"")
            .append(")"); 

        if(UDEnvConstants.isDebugMode) {
            Log.v(TAG, "addMessage = " + writeSqlBuilder.toString());
        }*/
    	
    	long rowId = -1;
    	
    	ContentValues cv = new ContentValues();
    	cv.put(UDIMDBHelper.COLUMN_TYPE, message.type);
    	cv.put(UDIMDBHelper.COLUMN_TEXT_URL,message.text_url);
    	cv.put(UDIMDBHelper.COLUMN_THUMBNAIL_PATH,message.thumbnailPath);


        SQLiteDatabase db = helper.getWritableDatabase();
        db.beginTransaction();
        try {
        	rowId = db.insert(UDIMDBHelper.TABLE_NAME, null, cv);//.execSQL(writeSqlBuilder.toString());
            db.setTransactionSuccessful();
        }catch(Exception e){
        	e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        
        try {
            db.close();
            db = null;
		} catch (Exception e) {
		}

        
        
        return rowId;
    }


    public void markFlag(long rowId, boolean success){
    	  ContentValues cv = new ContentValues();
    	  cv.put(UDIMDBHelper.COLUMN_SEND_STATE, success?UDIMDBHelper.RESULT_SUCCESS:UDIMDBHelper.RESULT_FAIL);
    	  
    	  SQLiteDatabase db = helper.getReadableDatabase();
    	  db.update(UDIMDBHelper.TABLE_NAME, cv, UDIMDBHelper.COLUMN_ID+" = "+rowId, null);
    	  
          db.close();
          db = null;
    }

    /**
     * 获取指定条数的聊天记录
     * @return
     */
    public synchronized ArrayList<UDIMMessage> getMessages(int offset) {
        StringBuilder sqlBuilder = new StringBuilder("select * from ")
           .append(UDIMDBHelper.TABLE_NAME)
           .append(" order by ")
           .append(UDIMDBHelper.COLUMN_ID)
           .append(" limit ").append(HISTORY_COUNT)
           .append(" offset ").append(offset);

        if(UDEnvConstants.isDebugMode) {
            Log.v(TAG, "getAllMessage = " + sqlBuilder.toString());
        }

        ArrayList<UDIMMessage> list = new ArrayList<UDIMMessage>();

        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cur = db.rawQuery(sqlBuilder.toString(), null);
        if(cur != null) {
            while(cur.moveToNext()) {
                UDIMMessage message = new UDIMMessage();
                message.type = cur.getInt(cur.getColumnIndex(UDIMDBHelper.COLUMN_TYPE));
                message.text_url = cur.getString(cur.getColumnIndex(UDIMDBHelper.COLUMN_TEXT_URL));
                message.thumbnailPath = cur.getString(cur.getColumnIndex(UDIMDBHelper.COLUMN_THUMBNAIL_PATH));
                message.id=cur.getLong( cur.getColumnIndex(UDIMDBHelper.COLUMN_ID));
                message.state = cur.getInt( cur.getColumnIndex(UDIMDBHelper.COLUMN_SEND_STATE));

                list.add(message);
            }
            cur.close();
            cur = null;
        }
        db.close();
        db = null;

        return list;
    }


    /**
     * 获取消息数量
     * @return
     */
    public synchronized int getMessageCount() {
        int count = 0;
        StringBuilder sqlBuilder = new StringBuilder("select ")
                .append(UDIMDBHelper.COLUMN_ID)
                .append(" from ")
                .append(UDIMDBHelper.TABLE_NAME);

        if (UDEnvConstants.isDebugMode) {
            Log.v(TAG, "getMessageCount = " + sqlBuilder.toString());
        }

        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cur = db.rawQuery(sqlBuilder.toString(), null);
        if(cur != null) {
            count = cur.getCount();
            cur.close();
            cur = null;
        }
        
        db.close();
        db = null;

        return count;
    }


}
