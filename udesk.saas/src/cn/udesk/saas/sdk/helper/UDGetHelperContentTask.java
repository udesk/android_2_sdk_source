/**
 * 获取帮助信息
 * @author xutao
 */
package cn.udesk.saas.sdk.helper;

import org.json.JSONObject;

import android.os.AsyncTask;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import cn.udesk.saas.sdk.utils.UDEnvConstants;
import cn.udesk.saas.sdk.utils.UDHttpUtil;
import cn.udesk.saas.sdk.utils.UDUtils;

public class UDGetHelperContentTask extends AsyncTask<String, Integer, String> {

	private OnGetHelperContentListener mListener;
    private boolean isRunning = true;

    public UDGetHelperContentTask(OnGetHelperContentListener listener) {
        mListener = listener;
        isRunning = true;
    }

    public void cancelled() {
        isRunning = false;
        cancel(true);
    }

    @Override
    protected String doInBackground(String... params) {

        String subDomain = params[0];
        String secretKey = params[1];
        String id = params[2];

        String url = subDomain;
        if(!subDomain.startsWith("http://")) {
            url = "http://" + subDomain;
        }

        url = url + "/api/v1/articles/" + id + ".json?sign=" + UDUtils.MD5(secretKey);

        if(UDEnvConstants.isDebugMode) {
            Log.w("UDGetKownlegeContentTask", url);
        }

        return UDHttpUtil.get(url);
    }


    @Override
    protected void onPostExecute(String result) {
        if(UDEnvConstants.isDebugMode) {
            Log.w("UDGetKownlegeContentTask", "result = " + result);
        }

        if(!isRunning) {
            return;
        }

        try {
            if(TextUtils.isEmpty(result)) {
                mListener.onFail(null);
                return;
            }

            JSONObject json = new JSONObject(result);
            int status = json.optInt("status");

            if(status == 0) {
                JSONObject contents = json.optJSONObject("contents");
                if(contents == null) {
                    mListener.onFail(null);
                } else {
                    String subject = contents.optString("subject");
                    String content = contents.optString("content");
                    int start = 0, end = 0;
                    while ((start = content.indexOf("<img")) > -1) {
                        end = content.indexOf(">", start);
                        content = content.substring(0, start) + content.substring(end + 1);
                    }
                    if(!TextUtils.isEmpty(content)) {
                        content = Html.fromHtml(content).toString();
                    }
                    mListener.onSucess(subject, content);
                }
            } else {
                mListener.onFail(json.optString("message"));
            }

        } catch(Exception e) {
            mListener.onFail(null);
        }
    }


    public interface OnGetHelperContentListener {

        public void onSucess(String subject, String content);

        public void onFail(String result);

    }

}
