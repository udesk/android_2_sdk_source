/**
 * IM 所有逻辑
 *
 * @author xutao
 */
package cn.udesk.saas.sdk.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.udesk.saas.sdk.UDeskSDK;
import cn.udesk.saas.sdk.adapter.UDEmojiAdapter;
import cn.udesk.saas.sdk.adapter.UDIMChatAdatper;
import cn.udesk.saas.sdk.chat.UDDownloadTask;
import cn.udesk.saas.sdk.chat.UDIMMessage;
import cn.udesk.saas.sdk.db.UDIMDBHelper;
import cn.udesk.saas.sdk.db.UDIMDBManager;
import cn.udesk.saas.sdk.manager.UDUserManager;
import cn.udesk.saas.sdk.utils.CommonUtil;
import cn.udesk.saas.sdk.utils.UDEnvConstants;
import cn.udesk.saas.sdk.utils.UDHttpUtil;
import cn.udesk.saas.sdk.utils.UDUtils;
import cn.udesk.saas.sdk.view.UDPullGetMoreListView;
import cn.udesk.saas.sdk.view.UDPullGetMoreListView.OnRefreshListener;
import cn.udesk.saas.sdk.view.UdeskConfirmPopWindow;
import cn.udesk.saas.sdk.view.UdeskConfirmPopWindow.OnPopConfirmClick;
import cn.udesk.saas.sdk.view.UdeskMultiMenuHorizontalWindow;
import cn.udesk.saas.sdk.view.UdeskMultiMenuHorizontalWindow.OnPopMultiMenuClick;
import cn.udesk.saas.sdk.voice.AudioRecordState;
import cn.udesk.saas.sdk.voice.VoiceRecord;
import cn.udesk.saas.sdk.voice.wav.AudioRecordingWavThread;

import com.qiniu.android.http.ResponseInfo;
import com.qiniu.android.storage.UpCompletionHandler;
import com.qiniu.android.storage.UpProgressHandler;
import com.qiniu.android.storage.UploadManager;
import com.qiniu.android.storage.UploadOptions;

public class UDIMFragment extends UDeskFragment implements OnClickListener, OnItemClickListener,View.OnTouchListener   {
    
    private final static String TAG =UDIMFragment.class.getSimpleName();
    
    public final static String TAG_SHOWPICTURE="showPicture" ;
    public final static String TAG_SHOWEMOTION =  "showEmotion" ;

    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 101;
    private static final int SELECT_IMAGE_ACTIVITY_REQUEST_CODE = 102;

    private UDXmppThread xmppThread = null;

    private static Uri photoUri;

    private UDPullGetMoreListView lvConversation;
    private UDIMChatAdatper mChatAdapter;

    private GridView gvEmojis;
    private UDEmojiAdapter mEmojiAdapter;

    private LinearLayout llLoading, llContent, lloptions, llemojis, llUpload;
    private RelativeLayout llBottom;
    private EditText  etMessage;
    private ImageView ivOptions, ivEmojis;
    private TextView  tvNaviLeft, tvNaviTitle,  tvUpload;
    private TextView tvSend;//发送这个按钮集多重状态为一身，因此InputState的当前值赋给他作为Tag

    private Button btnPhoto, btnCamera;
    
    private View recordView,recordBackground;

    private long lastSessionTime = 0;

    private UDIMMessage currentUploadImageMessage = null;
    DateFormat mSdf = new SimpleDateFormat("MM月dd日 EE HH时mm分", Locale.CHINA);

    private int historyCount = 0, offset = -1;

    private Vector<String> downloadStack = new Vector<String>();
    private UDDownloadTask downloadTask;

    private VoiceRecord mVoiceRecord=null;

    private Handler mHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(android.os.Message msg) {
            final Activity activity = UDIMFragment.this.getActivity();
            if(activity==null || activity.isFinishing() || !Thread.currentThread().isAlive()) {
                return;
            }

            switch (msg.what) {
            case HandlerType.TYPE_CUSTOMER:
                String lastCSId = UDUtils.loadConfigString(activity, UDEnvConstants.PREF_LAST_CS);
                if(TextUtils.isEmpty(lastCSId)) {
                    Toast.makeText(activity, mResIDLoader.getResStringID( "udesk_not_find_customer_service"), Toast.LENGTH_LONG).show();
                    activity.finish();
                } else {
                    UDUserManager.getInstance().setCustomerServiceId(lastCSId, false);
                    mHandler.sendEmptyMessage(HandlerType.TYPE_START_XMMP);
                }
                break;

            case HandlerType.TYPE_START_XMMP:

                if(UDEnvConstants.isDebugMode) Log.w(TAG, UDUserManager.getInstance().getCustomerServiceId());

                if(!TextUtils.isEmpty(UDUserManager.getInstance().getCustomerServiceId())) {
                    UDUtils.saveConfigString(activity,  UDEnvConstants.PREF_LAST_CS,
                            UDUserManager.getInstance().getCustomerServiceId());
                }

                if(xmppThread == null) {
                    xmppThread = new UDXmppThread(this);
                }
                xmppThread.start();
                break;

            case HandlerType.TYPE_STATE:
                if(UDUserManager.getInstance().isCustomerServiceOnline()) {
                    tvNaviTitle.setText(mResIDLoader.getResStringID( "udesk_navi_im_title_online"));
                } else {
                    tvNaviTitle.setText(mResIDLoader.getResStringID( "udesk_navi_im_title_offline"));
                    confirmToForm();
                }

                loadHistoryRecords();

                llContent.setVisibility(View.VISIBLE);
                llLoading.setVisibility(View.GONE);
                break;

            case 1:  // 有新消息
                if(msg.obj instanceof UDIMMessage) {
                	tvNaviTitle.setText(mResIDLoader.getResStringID( "udesk_navi_im_title_online"));
                	
                    UDIMMessage udMessage = (UDIMMessage)msg.obj;

                    if((udMessage.type & UDIMMessage.TYPE_IMAGE) == UDIMMessage.TYPE_IMAGE) {
                        addDownloadTask(udMessage.text_url);
                    } else {
                        showNewTime();

                        UDIMDBManager.getInstance().addMessageForDB(udMessage);

                        mChatAdapter.addItem((UDIMMessage)msg.obj);
                        lvConversation.smoothScrollToPosition(mChatAdapter.getCount());
                    }
                }
                break;

            case 2:  // 状态更新
                if(msg.obj instanceof String) {
                    boolean isOnline = Boolean.parseBoolean((String)msg.obj);
                    if(isOnline) {
                        tvNaviTitle.setText(mResIDLoader.getResStringID( "udesk_navi_im_title_online"));
                    } else {
                        tvNaviTitle.setText(mResIDLoader.getResStringID( "udesk_navi_im_title_offline"));
                    }
                }

                break;

            case 11:  // 图片下载成功

                if(msg.obj instanceof String) {

                    showNewTime();

                    UDIMMessage udMessage = new UDIMMessage();
                    udMessage.thumbnailPath = (String)msg.obj;
                    udMessage.type = UDIMMessage.TYPE_IMAGE;

                    UDIMDBManager.getInstance().addMessageForDB(udMessage);

                    mChatAdapter.addItem(udMessage);
                    lvConversation.smoothScrollToPosition(mChatAdapter.getCount());

                } else {
                    Toast.makeText(activity, mResIDLoader.getResStringID( "udesk_download_image_fail"), Toast.LENGTH_SHORT).show();
                }

                startNextDownload();

                break;

            case 12:  // 图片下载失败
                Toast.makeText(activity, mResIDLoader.getResStringID( "udesk_download_image_fail"), Toast.LENGTH_SHORT).show();
                startNextDownload();
                break;

            default:
                break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        
        return inflater.inflate(mResIDLoader.getResLayoutID("udesk_activity_im"), container, false);
    }


    @SuppressLint("NewApi")
    @Override
    public void onViewCreated(View rootView, Bundle savedInstanceState) {
        tvNaviLeft = (TextView) rootView.findViewById(mResIDLoader.getResIdID("udesk_navi_left"));
        if((UDUserManager.getInstance().getSDKMode() & UDeskSDK.MODE_HELPER) == UDeskSDK.MODE_HELPER) {
            tvNaviLeft.setText(mResIDLoader.getResStringID( "udesk_navi_back"));
        } else {
            tvNaviLeft.setText(mResIDLoader.getResStringID( "udesk_navi_close"));
        }
        tvNaviLeft.setOnClickListener(this);

        tvNaviTitle = (TextView)rootView.findViewById(mResIDLoader.getResIdID("udesk_navi_title"));
        tvNaviTitle.setText(mResIDLoader.getResStringID( "udesk_navi_im_title_offline"));

        llLoading = (LinearLayout)rootView.findViewById(mResIDLoader.getResIdID("udesk_im_loading"));

        llContent = (LinearLayout)rootView.findViewById(mResIDLoader.getResIdID("udesk_im_content"));
        
        recordView =rootView.findViewById(mResIDLoader.getResIdID("udesk_record"));
        recordBackground =rootView.findViewById(mResIDLoader.getResIdID("udesk_recore_bg_view"));
        

        mChatAdapter = new UDIMChatAdatper(this.getActivity());

        lvConversation = (UDPullGetMoreListView)rootView.findViewById(mResIDLoader.getResIdID("udesk_conversation"));
        lvConversation.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        lvConversation.setAdapter(mChatAdapter);
        lvConversation.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadHistoryRecords();
            }
        });
        lvConversation.setOnItemClickListener(this);

        llBottom = (RelativeLayout)rootView.findViewById(mResIDLoader.getResIdID("udesk_bottom"));

        tvSend = (TextView)rootView.findViewById(mResIDLoader.getResIdID("udesk_bottom_send"));
        tvSend.setOnClickListener(this);

        etMessage = (EditText)rootView.findViewById(mResIDLoader.getResIdID("udesk_bottom_input"));

        llUpload = (LinearLayout)rootView.findViewById(mResIDLoader.getResIdID("udesk_bottom_upload"));
        tvUpload = (TextView)rootView.findViewById(mResIDLoader.getResIdID("udesk_upload_hint"));

        // check picture option
        ivOptions = (ImageView)rootView.findViewById(mResIDLoader.getResIdID("udesk_bottom_show_option"));
        boolean isShowPicture = this.getArguments()!=null && getArguments().getBoolean (TAG_SHOWPICTURE, true);
        if(isShowPicture){
            ivOptions.setVisibility(View.VISIBLE);
            ivOptions.setOnClickListener(this);
        }else{
            setParentLeft(rootView.findViewById(mResIDLoader.getResIdID("udesk_bottom_input")));
            setParentLeft(rootView.findViewById(mResIDLoader.getResIdID("udesk_recordView")));
        }

        // show emoji 
        ivEmojis = (ImageView)rootView.findViewById(mResIDLoader.getResIdID("udesk_bottom_show_emoji"));
        boolean isShowEmoji =this.getArguments()!=null && getArguments().getBoolean(TAG_SHOWEMOTION, true);
        if(isShowEmoji){
        	ivEmojis.setVisibility(View.VISIBLE);
            ivEmojis.setOnClickListener(this);
        }else{
            ivEmojis.setTag(false);//表示不需要显示Emojis. 后面的代码会读取这个值进行状态判断
        }

        llemojis = (LinearLayout)rootView.findViewById(mResIDLoader.getResIdID("udesk_bottom_emojis"));

        mEmojiAdapter = new UDEmojiAdapter(this.getActivity());
        gvEmojis = (GridView)rootView.findViewById(mResIDLoader.getResIdID("udesk_bottom_emoji_pannel"));
        gvEmojis.setAdapter(mEmojiAdapter);
        gvEmojis.setOnItemClickListener(this);

        lloptions = (LinearLayout)rootView.findViewById(mResIDLoader.getResIdID("udesk_bottom_options"));

        btnCamera = (Button)rootView.findViewById(mResIDLoader.getResIdID("udesk_bottom_option_camera"));
        btnCamera.setOnClickListener(this);

        btnPhoto = (Button)rootView.findViewById(mResIDLoader.getResIdID("udesk_bottom_option_photo"));
        btnPhoto.setOnClickListener(this);

        UDIMDBManager.getInstance().init(this.getActivity());
        historyCount = UDIMDBManager.getInstance().getMessageCount();

        UDUserManager.getInstance().login(this.getActivity(), mHandler);

        
        // about input state
        mTextReadyState.setNextState(mRecordViewInputState);
        mRecordViewInputState.setNextState(mTextReadyState);
        tvSend.setTag(mTextReadyState);
        mTextReadyState.work();
        

        etMessage.setOnTouchListener(this);
        // 监听内容变化
        etMessage.addTextChangedListener(new TextWatcher(){
            private int mLastContentLength=0;
            public void afterTextChanged(Editable arg0) { }
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {  }
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
               int newLength= etMessage.getText().toString().length();
               if(mLastContentLength!=newLength){
                   if(newLength==0){
                       mTextReadyState.work();
                       tvSend.setTag(mTextReadyState);
                   }else{
                       mEditingState.work();
                       tvSend.setTag(mEditingState);
                   }
                   
                   mLastContentLength= newLength;
               }
            }
        });
    }

    private void setParentLeft(View view){
      RelativeLayout.LayoutParams rllp = (RelativeLayout.LayoutParams)  view.getLayoutParams();
    //  rllp.removeRule(RelativeLayout.RIGHT_OF); 低版本么有removeRule这个Api
      rllp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
    }

    @Override
    public void onDestroyView() {
        mHandler.removeCallbacksAndMessages(null);
        super.onDestroyView();
    }
   private void changeImState(long id ){
	   UDPullGetMoreListView lvCTmp = lvConversation;
	   for (int i = lvCTmp.getChildCount()-1; i>=0; i--) {
		  View child = lvCTmp.getChildAt(i);
		  if(child!=null){
			 if( mChatAdapter.changeImState(child,id)){
				 return;
			 }
		  }
	}
   }

   private boolean readyForSendMessage(final Activity activity){
       String text = etMessage.getText().toString().trim();
       if(TextUtils.isEmpty(text)) {
           Toast.makeText(activity, mResIDLoader.getResStringID( "udesk_send_message_empty"), Toast.LENGTH_SHORT).show();
           return false;
       }
       
       if(!UDHttpUtil.isNetworkConnected(activity )){
           Toast.makeText(activity, mResIDLoader.getResStringID( "udesk_error_network"), Toast.LENGTH_SHORT).show();
           return false;
       }else{
           long rowId = sendTextMessage(text);
           changeImState(rowId);
           if(rowId>0){//大于0表示成功
               etMessage.setText("");
               return true;
           }else{
               return false;
           }
       }
   }
   
   private void switchEmoji(){
       if(lloptions.getVisibility() == View.VISIBLE) {
           lloptions.setVisibility(View.GONE);
       }

       if(llemojis.getVisibility() == View.VISIBLE) {
           llemojis.setVisibility(View.GONE);
           if(etMessage.isFocused()){
               CommonUtil.showSoftKeyboard(getActivity(), etMessage);
           }
       } else {
           llemojis.setVisibility(View.VISIBLE);
           CommonUtil.hideSoftKeyboard (getActivity(), etMessage);
       }
   }
    @Override
    public void onClick(View v) {
      final Activity activity=  this.getActivity();
      if(activity==null){
          return;
      }
      
        if(v.getId() == mResIDLoader.getResIdID("udesk_bottom_send")) {
           InputState state=(InputState) v.getTag();
           if(state instanceof EditingState){
               // 我们没有为EditingState 设置 Next State
               if(readyForSendMessage(activity)){
                   v.setTag(mTextReadyState);
                   mTextReadyState.work();
               }
           }else{
               InputState nextState=state.getNextState();
               nextState.work();
               v.setTag(nextState);
           }
            

        } else if(v.getId() == mResIDLoader.getResIdID("udesk_bottom_show_option")) {

            if(TextUtils.isEmpty(UDUserManager.getInstance().getQiniuToken())) {
                Toast.makeText(activity, mResIDLoader.getResStringID( "udesk_upload_image_forbidden"), Toast.LENGTH_LONG).show();
                return;
            }

            if(llemojis.getVisibility() == View.VISIBLE) {
                llemojis.setVisibility(View.GONE);
            }

            if(lloptions.getVisibility() == View.VISIBLE) {
                lloptions.setVisibility(View.GONE);
            } else {
                lloptions.setVisibility(View.VISIBLE);
            }

        } else if(v.getId() == mResIDLoader.getResIdID("udesk_bottom_show_emoji")) {
            switchEmoji();
        } else if(v.getId() == mResIDLoader.getResIdID("udesk_navi_left")) {
            closeConnection();
            activity.finish();
        } else if(v.getId() == mResIDLoader.getResIdID("udesk_bottom_option_camera")) {
            takePhoto();
            lloptions.setVisibility(View.GONE);
        } else if(v.getId() == mResIDLoader.getResIdID("udesk_bottom_option_photo")) {
            selectPhoto();
            lloptions.setVisibility(View.GONE);
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if(UDEnvConstants.isDebugMode) {
            Log.w(TAG, "onItemClick position:" + position + " id:" + id);
        }

        if(parent == lvConversation) {
            final UDIMMessage message = mChatAdapter.getItem((int)id);
            //先判断下是否发送失败，失败就需要重发
            if(  (message.type & UDIMMessage.TYPE_MINE) == UDIMMessage.TYPE_MINE&& message.state!=UDIMDBHelper.RESULT_SUCCESS
            	&&	((message.type & UDIMMessage.TYPE_IMAGE) == UDIMMessage.TYPE_IMAGE  || (message.type & UDIMMessage.TYPE_TEXT) == UDIMMessage.TYPE_TEXT)  ){
            	if(mChatAdapter.isRetryClick(view, lvConversation.getRealPosition())){
            		confirmRetry(message);
            		return;
            	}else{
            		// do nothing //如果没有点中 重试按钮，则不予考虑重试。 按照点击事件  进行下一步处理
            	}
            }

            // 这个应该是发送成功的情况下
            if((message.type & UDIMMessage.TYPE_IMAGE) == UDIMMessage.TYPE_IMAGE) {
            	previewPhoto(message);
            }else if((message.type & UDIMMessage.TYPE_TEXT) == UDIMMessage.TYPE_TEXT){
            	handleText(message,mChatAdapter.getTextViewForMyOrCs(view));
            }

        } else if(parent == gvEmojis) {
            String text = etMessage.getText().toString();
            int selection = etMessage.getSelectionStart();
            String emoji = mEmojiAdapter.getItem((int)id);

            if(selection == text.length()) {
                // 尾部添加
                text += emoji;
            } else {
                // 插入
                text = text.substring(0, selection) + emoji + text.substring(selection);
            }

            
            final Activity activity=  this.getActivity();
            if(activity==null){
                return;
            }
            etMessage.setText(UDEmojiAdapter.replaceEmoji(activity, text));
            etMessage.setSelection(selection + emoji.length());
        }

    }
    
    private void confirmRetry(final UDIMMessage message){
        final Activity activity=  this.getActivity();
        if(activity==null){
            return;
        }
        
        
    	UdeskConfirmPopWindow confirmWindow = new UdeskConfirmPopWindow(activity);
    	confirmWindow.show(activity , activity.getWindow().getDecorView(), this.getString(mResIDLoader.getResStringID("udesk_retry")), 
    			this.getString(mResIDLoader.getResStringID("udesk_cancel")),this.getString(mResIDLoader.getResStringID("udesk_msg_retry")), new OnPopConfirmClick(){
					public void onPositiveClick() {
						doRetry(message);
					}

					@Override
					public void onNegativeClick() {
					}
    		
    	});
    }
    private void doRetry(final UDIMMessage message){
    	// 界面准备
       	message.state = UDIMDBHelper.RESULT_WAIT;
    	changeImState(message.id);
    	//尝试发送
    	mHandler.postDelayed(new Runnable() {
			public void run() {
            	retryMessageNoUI(message);
            	changeImState(message.id);
			}
		}, 1000);// 我们的重试比较快，现在没有时间实现复杂的重试逻辑. 延迟下是为了 让用户看到进度条，以为咱们真的已经在重试。 20150512

    	return;
    }
    private void handleText(final UDIMMessage message,View targetView){
        final Activity activity=  this.getActivity();
        if(activity==null){
            return;
        }

        String[] menuLabel = new String[]{ this.getString(mResIDLoader.getResStringID("udesk_copy")) };
        UdeskMultiMenuHorizontalWindow  menuWindow = new UdeskMultiMenuHorizontalWindow(activity);
        menuWindow.show( activity, targetView, menuLabel, new OnPopMultiMenuClick() {
			public void onMultiClick(int MenuIndex) {
		        if(MenuIndex==0){
		        	doCopy(message.text_url);
		        }
			}
		});
    }

    @SuppressLint("NewApi")
	private void doCopy(String content){
        final Activity activity=  this.getActivity();
        if(activity==null){
            return;
        }
        

    	if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.HONEYCOMB) {
    		android.content.ClipboardManager c = (android.content.ClipboardManager) activity.getSystemService(Activity.CLIPBOARD_SERVICE);
    		  c.setPrimaryClip(ClipData.newPlainText(null,content));
    	}else{
    		android.text.ClipboardManager c = (android.text.ClipboardManager) activity.getSystemService(Activity.CLIPBOARD_SERVICE);
    		c.setText(content);
    	}
    }
    private void previewPhoto(UDIMMessage message){
        if(!TextUtils.isEmpty(message.thumbnailPath)) {
            String sourceImagePath;
            if((message.type & UDIMMessage.TYPE_MINE) == UDIMMessage.TYPE_MINE) {
                sourceImagePath = message.thumbnailPath.replace("_thumbnail", "_upload");
            } else {
                sourceImagePath = message.thumbnailPath.replace("_thumbnail", "_download");
            }

            if(UDEnvConstants.isDebugMode) {
                Log.w(TAG, sourceImagePath);
            }

            File sourceFile = new File(sourceImagePath);
            if(sourceFile.exists()) {
                Intent intent = new Intent(UDIMFragment.this.getActivity(),UDeskBaseActivity.class);
                intent.putExtra(UDeskBaseActivity.FRAGMENTS_TAG,UDeskZoomImageViewFragment.class.getName());
                
                Bundle data= new Bundle();
                data.putParcelable("image_path", Uri.fromFile(sourceFile));
                intent.putExtra(UDeskBaseActivity.DATA_TAG, data);
                //intent.setAction(android.content.Intent.ACTION_VIEW);
               // intent.setDataAndType(Uri.fromFile(sourceFile), "image/*");
              //  "android:fragments"
                startActivity(intent);
            }
        }
    }

    @Override
    public boolean onBackPressed() {
        closeConnection();
        return false;
    }


    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if(view==etMessage){
            if(event.getAction()==MotionEvent.ACTION_DOWN){
                checkEditEvent();
            }
        }
        return false;
    }
    private void checkEditEvent( ){
        if(llemojis.getVisibility() == View.VISIBLE) {
            llemojis.setVisibility(View.GONE);
        }
    }
    
    private void confirmToForm(){
        final Activity activity=  this.getActivity();
        if(activity==null){
            return;
        }
        
        UdeskConfirmPopWindow confirmWindow = new UdeskConfirmPopWindow(activity);
        confirmWindow.show(activity , activity.getWindow().getDecorView(), this.getString(mResIDLoader.getResStringID("udesk_ok")), 
                this.getString(mResIDLoader.getResStringID("udesk_cancel")),this.getString(mResIDLoader.getResStringID("udesk_msg_offline_to_form")), new OnPopConfirmClick(){
                    public void onPositiveClick() {
                        goToForm(activity);
                    }

                    @Override
                    public void onNegativeClick() {}
            
        });
    }
    
    private void goToForm(final Activity activity){
        Intent intent = new Intent(activity,UDeskBaseActivity.class);
        intent.putExtra(UDeskBaseActivity.FRAGMENTS_TAG, UDeskFormFragment.class.getName());
 
        this.startActivity(intent);
    }

    private void loadHistoryRecords() {
        // 设置为Normal，加载更多之后，当前条目不变
        lvConversation.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);

        // 已经没有更早的数据了
        if(offset == 0) {
            Toast.makeText(UDIMFragment.this.getActivity(), mResIDLoader.getResStringID( "udesk_no_more_history"), Toast.LENGTH_SHORT).show();
            lvConversation.onRefreshComplete();
            lvConversation.setSelection(0);
        } else {
            // 还有老数据
            if(offset == -1) {
                offset = historyCount - UDIMDBManager.HISTORY_COUNT;
            } else {
                offset = offset - UDIMDBManager.HISTORY_COUNT;
            }
            offset = (offset < 0 ? 0 : offset);
            ArrayList<UDIMMessage> list = UDIMDBManager.getInstance().getMessages(offset);

            mChatAdapter.addHistoryArray(list);
            lvConversation.onRefreshComplete();
            lvConversation.setSelection(list.size());
        }

        // 延迟1秒设为自动滚动到最下
        lvConversation.postDelayed(new Runnable() {
            @Override
            public void run() {
                lvConversation.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
            }
        }, 1000);
    }


   /**
    * 您要发送的消息,这里如果成功返回数据的rowId。 如果失败，则返回-1
    * @param text
    * @return
    */
      long  sendTextMessage(String text) {
        showNewTime();

        UDIMMessage udMessage = new UDIMMessage();
        udMessage.type = UDIMMessage.TYPE_MINE | UDIMMessage.TYPE_TEXT;
        udMessage.text_url = text;

        final long rowId =UDIMDBManager.getInstance().addMessageForDB(udMessage);

        udMessage.id = rowId;
        mChatAdapter.addItem(udMessage);
        lvConversation.smoothScrollToPosition(mChatAdapter.getCount());

        if(xmppThread != null) {
        	boolean success = xmppThread.sendMessage(UDIMMessage.TYPE_TEXT, text,rowId);
        	udMessage.state =  success?UDIMDBHelper.RESULT_SUCCESS:UDIMDBHelper.RESULT_FAIL;
         return    success?rowId:-1;
        }

        udMessage.state = UDIMDBHelper.RESULT_WAIT;
        return -1;
    }
      
    boolean  retryMessageNoUI( UDIMMessage udMessage){
    	if(xmppThread != null) {
        	boolean success =  xmppThread.sendMessage(udMessage.type & UDIMMessage.TYPE_MINE, udMessage.text_url,udMessage.id);
        	udMessage.state =  success?UDIMDBHelper.RESULT_SUCCESS:UDIMDBHelper.RESULT_FAIL;
        	return success;
        }
    	
    	return false;
    }

    /**
     * 关闭连接
     */
    private void closeConnection() {
        if(xmppThread != null) {
            xmppThread.cancel();
            xmppThread = null;
        }
    }


    /**
     * 显示时间
     */
    private void showNewTime() {
        // 5分钟
        if(System.currentTimeMillis() - lastSessionTime >= 1000 * 60 * 1) {

            lastSessionTime = System.currentTimeMillis();

            UDIMMessage udMessage = new UDIMMessage();
            udMessage.type = UDIMMessage.TYPE_TIME;
            udMessage.text_url = mSdf.format(new Date(lastSessionTime));
            udMessage.id =-1;//这个值对于时间类型是没有意义的
            udMessage.state=-1;//这个值对于时间类型是没有意义的

            udMessage.id = UDIMDBManager.getInstance().addMessageForDB(udMessage);

            mChatAdapter.addItem(udMessage);
        } else {
            lastSessionTime = System.currentTimeMillis();
        }
    }


    /**
     * 添加图片下载任务
     * @param downloadUrl
     */
    private void addDownloadTask(String downloadUrl) {
        if(!downloadStack.contains(downloadUrl)) {
            downloadStack.add(downloadUrl);
        }

        statDownloadTask(downloadUrl);
    }


    /**
     * 在完成一个下载之后，启动另一个下载
     */
    private void startNextDownload() {
        downloadTask = null;

        if(downloadStack.size() > 0) {
            downloadStack.remove(0);
        }
        if(downloadStack.size() > 0) {
            statDownloadTask(downloadStack.firstElement());
        } else {
            llBottom.setVisibility(View.VISIBLE);
            llUpload.setVisibility(View.GONE);
        }
    }

    /**
     * 启动下载
     * @param downloadUrl
     */
    private void statDownloadTask(String downloadUrl) {
        if(downloadTask == null) {
            llBottom.setVisibility(View.GONE);
            llUpload.setVisibility(View.VISIBLE);
            tvUpload.setText(mResIDLoader.getResStringID( "udesk_download_image"));

            downloadTask = new UDDownloadTask(this.getActivity(), downloadUrl, mHandler);
            downloadTask.execute();
        }
    }


    //########################################################################

    /**
     * 选择要上传的照片
     */
    private void selectPhoto() {
        Intent intent=new Intent(Intent.ACTION_GET_CONTENT);// ACTION_OPEN_DOCUMENT
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/jpeg");
        startActivityForResult(intent, SELECT_IMAGE_ACTIVITY_REQUEST_CODE);
    }


    /**
     * 拍照
     */
    private void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        photoUri = UDUtils.getOutputMediaFileUri();
        // 此处这句intent的值设置关系到后面的onActivityResult中会进入那个分支，即关系到data是否为null，如果此处指定，则后来的data为null
        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE == requestCode) {
            if (Activity.RESULT_OK == resultCode) {
                // Check if the result includes a thumbnail Bitmap
                if (data != null) {
                    // 没有指定特定存储路径的时候
                    // 指定了存储路径的时候（intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);）
                    // Image captured and saved to fileUri specified in the Intent
                    if (data.hasExtra("data")) {
                        Bitmap thumbnail = data.getParcelableExtra("data");

                        if(UDEnvConstants.isDebugMode) {
                            Log.e(TAG, "data != null");
                        }
                        if(thumbnail != null) {
                            sendBitmapMessage(thumbnail);
                        }
                    }
                } else {
                    // If there is no thumbnail image data, the image
                    // will have been stored in the target output URI.
                    // Resize the full image to fit in out image view.

                    if(UDEnvConstants.isDebugMode) {
                        Log.e(TAG, "data == null");
                    }

                    sendBitmapMessage(photoUri.getPath());

                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // User cancelled the image capture
            } else {
                // Image capture failed, advise user
            }
        } else if(SELECT_IMAGE_ACTIVITY_REQUEST_CODE == requestCode) {

            if(resultCode != Activity.RESULT_OK || data == null) {
                return;
            }

            Uri mImageCaptureUri = data.getData();

            if(UDEnvConstants.isDebugMode) {
                Log.e(TAG, "uri=" + mImageCaptureUri);
            }

            //返回的Uri不为空时，那么图片信息数据都会在Uri中获得。如果为空，那么我们就进行下面的方式获取
            if (mImageCaptureUri != null) {
                ContentResolver cr = this.getActivity().getContentResolver();
                try {
                    Bitmap bitmap = BitmapFactory.decodeStream(cr.openInputStream(mImageCaptureUri));
                    sendBitmapMessage(bitmap);
                } catch (FileNotFoundException e) {
                    Log.e("Exception", e.getMessage(),e);
                }
            } else {
                Bundle extras = data.getExtras();
                if (extras != null) {
                    //这里是有些拍照后的图片是直接存放到Bundle中的所以我们可以从这里面获取Bitmap图片
                    Bitmap bitmap = extras.getParcelable("data");
                    if (bitmap != null) {
                        sendBitmapMessage(bitmap);
                    }
                }
            }

        }

    }



    /**
     * 发送一条图片消息
     * @param bitmap
     */
    private void sendBitmapMessage(Bitmap bitmap) {
        if(bitmap == null) {
            return;
        }

        if(UDEnvConstants.isDebugMode) {
            Log.e(TAG, "sendBitmapMessage:h=" + bitmap.getHeight() + " w=" + bitmap.getWidth());
        }

        scaleBitmap(this.getActivity(), null, bitmap);

    }


    /**
     * 发送一条图片消息
     * @param bitmap
     */
    private void sendBitmapMessage(String photoPath) {
        if(UDEnvConstants.isDebugMode) {
            Log.w(TAG, "sendBitmapMessage : potoPath=" + photoPath);
        }

        scaleBitmap(this.getActivity(), photoPath, null);

    }


    /**
     * 七牛上传进度
     */
    private UpProgressHandler mUpProgressHandler = new UpProgressHandler() {
        public void progress(String key, double percent) {
            if(UDEnvConstants.isDebugMode) {
                Log.w(TAG, "UpCompletion : key=" + key + "  percent=" + percent);
            }
        }
    };


    /**
     * 七牛上传完成
     */
    private UpCompletionHandler mUpCompletionHandler = new UpCompletionHandler() {
        @Override
        public void complete(String key, ResponseInfo info, JSONObject response) {
            final Activity activity = UDIMFragment.this.getActivity();
            if(activity==null || !Thread.currentThread().isAlive() ||  activity.isFinishing()){
                return ;
            }

            // 可以点击其他任何按钮
            llBottom.setVisibility(View.VISIBLE);
            llUpload.setVisibility(View.GONE);

            if(null != response && response.has("key")) {
                if(UDEnvConstants.isDebugMode) {
                    Log.w(TAG, "UpCompletion : key=" + key + "\ninfo=" + info.toString() + "\nresponse=" + response.toString());
                }

                String qiniuKey = response.optString("key");
                String imageUrl = UDEnvConstants.UD_QINIU_UPLOAD_IMAGE + qiniuKey;

                // 显示病保存上传之后的图片消息
                if(currentUploadImageMessage != null) {
                    currentUploadImageMessage.text_url = imageUrl;

                    long rowId = UDIMDBManager.getInstance().addMessageForDB(currentUploadImageMessage);
                    currentUploadImageMessage.id = rowId;

                    mChatAdapter.addItem(currentUploadImageMessage);
                    lvConversation.smoothScrollToPosition(mChatAdapter.getCount());

                    if(xmppThread != null) {
                       boolean success = xmppThread.sendMessage(UDIMMessage.TYPE_IMAGE, imageUrl,rowId);
                       currentUploadImageMessage.state =  success?UDIMDBHelper.RESULT_SUCCESS:UDIMDBHelper.RESULT_FAIL;
                       changeImState(rowId);
                    }
                }

                currentUploadImageMessage = null;
                return;
            }

            Toast.makeText(UDIMFragment.this.getActivity(), mResIDLoader.getResStringID( "udesk_upload_image_error"), Toast.LENGTH_LONG).show();
        }
    };


    /**
     * 缩放图片
     * path和bitmap不会同时存在
     * @param context
     * @param path    图片存放路径
     * @param bitmap  图片实际内容
     */
    private void scaleBitmap(Context context, String path, Bitmap bitmap) {

        // 不可以在点击其他任何按钮
        llBottom.setVisibility(View.GONE);
        llUpload.setVisibility(View.VISIBLE);
        tvUpload.setText(mResIDLoader.getResStringID( "udesk_upload_image_process"));

        if(!TextUtils.isEmpty(path)) {
            new ScaleBitmapTask(context).execute(path);
        } else {
            new ScaleBitmapTask(context).execute(bitmap);
        }

    }


    /**
     * 缩放图片任务
     * 会把大图片缩放成
     * @author xutao
     *
     */
    private class ScaleBitmapTask extends AsyncTask<Object, Integer, String[]> {

        private int thumbnailMaxWH;

        public ScaleBitmapTask(Context context) {
            thumbnailMaxWH = (int)(context.getResources().getDisplayMetrics().density * 100);
        }

        @Override
        protected String[] doInBackground(Object... params) {
            try {

                Bitmap sourceImage = null, scaleImage = null, thumbnailImage = null;

                if(params[0] instanceof Bitmap) {
                    sourceImage = (Bitmap)params[0];
                } else if(params[0] instanceof String) {
                    sourceImage = BitmapFactory.decodeFile((String)params[0]);
                }
                if(sourceImage == null) {
                    return null;
                }

                int width = sourceImage.getWidth();
                int height = sourceImage.getHeight();
                int max = Math.max(width, height);

                BitmapFactory.Options factoryOptions = new BitmapFactory.Options();
                factoryOptions.inJustDecodeBounds = false;
                factoryOptions.inPurgeable = true;

                // 获取原图数据
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                sourceImage.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] data = stream.toByteArray();

                String imageName = UDUtils.MD5(data);

                File scaleImageFile = UDUtils.getOutputMediaFile(imageName + "_upload.jpg");
                if (!scaleImageFile.exists()) {
                    // 缩略图不存在，生成上传图
                    if(max > 1024) {
                        factoryOptions.inSampleSize = max / 1024;
                    } else {
                        factoryOptions.inSampleSize = 1;
                    }

                    FileOutputStream fos = new FileOutputStream(scaleImageFile);
                    scaleImage = BitmapFactory.decodeByteArray(data, 0, data.length, factoryOptions);
                    scaleImage.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                    fos.close();
                    fos = null;

                }

                File thumbnailFile = new File(scaleImageFile.getParent() + File.separator + imageName + "_thumbnail.jpg");
                if(!thumbnailFile.exists()) {

                    factoryOptions.inSampleSize = (int)Math.ceil(max / thumbnailMaxWH);

                    FileOutputStream fos = new FileOutputStream(thumbnailFile);
                    thumbnailImage = BitmapFactory.decodeByteArray(data, 0, data.length, factoryOptions);
                    thumbnailImage.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                    fos.close();
                    fos = null;

                }

                if(thumbnailImage != null) {
                    thumbnailImage.recycle();
                    thumbnailImage = null;
                }
                if(scaleImage != null) {
                    scaleImage.recycle();
                    scaleImage = null;
                }
                if(sourceImage != null) {
                    sourceImage.recycle();
                    sourceImage = null;
                }
                data = null;

                return new String[]{scaleImageFile.getPath(), thumbnailFile.getPath() };

            } catch (Exception e) {
                if (UDEnvConstants.isDebugMode) {
                    Log.e(TAG, e.toString());
                }
                return null;
            }

        }


        @Override
        protected void onPostExecute(String[] path) {
            final Activity activity= UDIMFragment.this.getActivity();
            if(activity==null ||   !Thread.currentThread().isAlive() || activity.isFinishing()){
                return;
            }
            

            if(TextUtils.isEmpty(path[0]) || TextUtils.isEmpty(path[1])) {
                // 可以进行其他操作
                llBottom.setVisibility(View.VISIBLE);
                llUpload.setVisibility(View.GONE);

                Toast.makeText(activity, mResIDLoader.getResStringID( "udesk_upload_image_error"), Toast.LENGTH_LONG).show();

            } else {
                tvUpload.setText(mResIDLoader.getResStringID( "udesk_upload_image"));

                // 上传原图
                UploadManager uploadManager = new UploadManager();
                uploadManager.put(  path[0],  null,  UDUserManager.getInstance().getQiniuToken(),   mUpCompletionHandler,
                        new UploadOptions(null, null, false, mUpProgressHandler, null));

                currentUploadImageMessage = new UDIMMessage();
                currentUploadImageMessage.type = UDIMMessage.TYPE_MINE | UDIMMessage.TYPE_IMAGE;
                currentUploadImageMessage.thumbnailPath = path[1];
            }
        }

    }

    /*******     about  record ****/
    private void recordStart() {
        mVoiceRecord = new AudioRecordingWavThread();
        mVoiceRecord.initResource("", new AudioRecordState() {
            @Override
            public void onRecordingError() {
                
            }
            
            @Override
            public void onRecordSuccess(final String resultFilePath) {
                
            }
            
            @Override
            public void onRecordSaveError() {
                
            }
        });
        mVoiceRecord.startRecord();
    }
    
    private void recordStop() {
        mVoiceRecord.stopRecord();
        mVoiceRecord = null;
    }
    /******  start: about input state *******/
    TextReadyState mTextReadyState = new TextReadyState();;
    RecordInputState mRecordViewInputState = new RecordInputState();
    EditingState mEditingState= new EditingState();
     abstract class InputState{ 
          private InputState mNextState;
           void setNextState(InputState nextState){
             mNextState = nextState;
           }
           
           InputState getNextState(){
               return mNextState;
           }
         
           abstract void work();
           void interWork(final int textVisibilty,final int recordViewVisibilty){
               // about edit message
               if(etMessage.getVisibility()!=textVisibilty){
                   etMessage.setVisibility(textVisibilty);
               }

               //  about emojis
               Object tag = ivEmojis.getTag();
               if(tag!=null && tag instanceof Boolean && !((Boolean)tag).booleanValue() ){
                   // do nothing
               }else{
                   if(ivEmojis.getVisibility()!=textVisibilty){
                       ivEmojis.setVisibility(textVisibilty);
                   }
               }

               // about recordView
               if(recordView.getVisibility()!=recordViewVisibilty){
                   recordView.setVisibility(recordViewVisibilty);
               }
               
               if(recordBackground.getVisibility()!=recordViewVisibilty){
                   recordBackground.setVisibility(recordViewVisibilty);
               }
           }
     }
     
     class TextReadyState extends InputState{
         void work(){
             interWork(View.VISIBLE,View.GONE);
             tvSend.setText(null);
             tvSend.setBackgroundResource(mResIDLoader.getResDrawableID("udesk_btn_record"));
         }
     }
     
     class RecordInputState extends InputState{
        void work(){
            interWork(View.GONE,View.VISIBLE);
            tvSend.setText(null);
            tvSend.setBackgroundResource(mResIDLoader.getResDrawableID("udesk_btn_keyboard"));
         }
     }

     class EditingState extends InputState{
         void work(){
             interWork(View.VISIBLE,View.GONE);
             tvSend.setText(UDIMFragment.this.getString(mResIDLoader.getResStringID("udesk_send_message")));
             tvSend.setBackgroundResource(mResIDLoader.getResDrawableID("udesk_im_send_selector"));
         }
     }
     
     /******  end: about input state *******/
}
