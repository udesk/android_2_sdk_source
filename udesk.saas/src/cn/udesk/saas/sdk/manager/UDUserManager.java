/**
 * 用户信息管理
 * @author xutao
 */
package cn.udesk.saas.sdk.manager;


import java.util.Map;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import cn.udesk.saas.sdk.OnUserInfoCallback;
import cn.udesk.saas.sdk.ResourcesIDLoader;
import cn.udesk.saas.sdk.UDeskSDK;
import cn.udesk.saas.sdk.chat.UDLoginTask;
import cn.udesk.saas.sdk.chat.UDUserInfoTask;
import cn.udesk.saas.sdk.utils.UDEnvConstants;
import cn.udesk.saas.sdk.utils.UDUtils;


public class UDUserManager {

    private int sdkMode = UDeskSDK.MODE_BOTH;
    private final static String TAG = UDUserManager.class.getSimpleName();

    /**
     * 第三方应用标识
     */
    private String subDomain = null;
    private String domain = null;
    private String subDomainName = null;
    private String secretKey = null;
    private String userId = null;
    private String nick = null;

    /**
     * xmpp 登录信息
     */
    private String loginName = null;
    private String loginPassword = null;
    private String loginServer = null;
    private String loginRoomId = null;
    private String qiniuToken = null;

    private boolean isOnline = false;
    private String customerServiceId = null;

    /**
     * 接口是单例形式的
     */
    private static UDUserManager instance = null;

    private UDUserManager() {
    }

    public static UDUserManager getInstance() {
        if(instance == null) {
            instance = new UDUserManager();
        }
        return instance;
    }


    //########################################################################
    /**
     *
     * @param mode
     */
    public void setSDKMode(int mode) {
        sdkMode = mode;
    }

    /**
     *
     * @return
     */
    public int getSDKMode() {
        return sdkMode;
    }

    //########################################################################
    /**
     *
     * @param subDomain
     */
    public void setSubDomain(String subDomain) {
        this.subDomain = subDomain;

        String[] dotStr = subDomain.split("\\.");
        subDomainName = dotStr[0];
        if(subDomainName.startsWith("http://")) {
            subDomainName = subDomainName.substring(6);
        } else if(subDomain.startsWith("https://")) {
            subDomainName = subDomainName.substring(7);
        }

        domain = subDomain.substring(subDomain.indexOf(".") + 1);

        if(UDEnvConstants.isDebugMode) {
            Log.w("UDUserManager", "subDomainName="+ subDomainName + "  domain=" + domain);
        }

    }

    /**
     *
     * @return
     */
    public String getSubDomain() {
        return subDomain;
    }

    public String getDomain() {
        return domain;
    }

    public void setSecretKey(String secret) {
        this.secretKey = secret;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setUserInfo(String uid, String nick) {
        this.userId = uid;
        this.nick = nick;
    }


    //########################################################################
    /**
     * 设置客服ID
     * @param csId
     */
    public void setCustomerServiceId(String csId, boolean isOnline) {
        this.customerServiceId = csId;
        this.isOnline = isOnline;
    }

    /**
     * 返回客服ID
     * @return
     */
    public String getCustomerServiceId() {
        return customerServiceId;
    }

    /**
     * 客服是否在线
     * @return
     */
    public boolean isCustomerServiceOnline() {
        return isOnline;
    }

    /**
     * 设置登录信息
     * @param name
     * @param password
     * @param server
     */
    public void setLoginInfo(String name, String password, String server, String roomId) {
        this.loginName = name;
        this.loginPassword = password;
        this.loginServer = server;
        this.loginRoomId = roomId;
    }

    /**
     * 返回登录名
     * @return
     */
    public String getLoginName() {
        return loginName;
    }

    /**
     * 返回登录密码
     * @return
     */
    public String getLoginPassword() {
        return loginPassword;
    }

    /**
     * 返回登录服务器信息
     * @return
     */
    public String getLoginServer() {
        return loginServer;
    }

    /**
     *
     * @return
     */
    public String getRoomId() {
        return loginRoomId;
    }

    /**
     * 登录
     * @param context
     */
    @SuppressLint("NewApi")
	public void login(Context context, Handler mHandler) {
        if(TextUtils.isEmpty(subDomain)) {
        	ResourcesIDLoader resIDLoader=ResourcesIDLoader.getResIdLoaderInstance(context);
            Toast.makeText(context, resIDLoader.getResStringID("udesk_domain_is_null"), Toast.LENGTH_LONG).show();
        } else {

            String udeskId = generateUdeskIdForServer(context);

            String csId = UDUtils.loadConfigString(context, UDEnvConstants.PREF_LAST_CS);

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
                new UDLoginTask(mHandler).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,subDomainName, secretKey, udeskId, nick, csId);
            }else{
            	new UDLoginTask(mHandler).execute ( subDomainName, secretKey, udeskId, nick, csId);
            }


        }

    }


    //########################################################################
    /**
     *
     * @param qiniu Token
     */
    public void setQiniuToken(String qnToken) {
        this.qiniuToken = qnToken;
    }

    /**
     * 获取七牛的Token
     * @return
     */
    public String getQiniuToken() {
        return qiniuToken;
    }

    private String generateUdeskIdForServer(Context context){
        String udeskId = userId;
        if(TextUtils.isEmpty(udeskId)) {

            String imei = UDUtils.getImei(context);
            String macAddress = UDUtils.getLocalMacAddress(context);

            if(TextUtils.isEmpty(imei) && TextUtils.isEmpty(macAddress)) {
                udeskId = UDUtils.MD5(String.valueOf(System.currentTimeMillis()));
            } else {
                udeskId = UDUtils.MD5(imei + macAddress);
            }
            
            userId = udeskId; 
        }
        
        return udeskId;
    }
    
    private String generateUdeskNickNameForServer(Context context){
        String nickName = nick;
        if(TextUtils.isEmpty(nickName)) {
        	ResourcesIDLoader resIDLoader=ResourcesIDLoader.getResIdLoaderInstance(context);
        	nickName= (context.getResources().getString(resIDLoader.getResStringID("udesk_nick_anonymous"))+System.currentTimeMillis());
        	nick = nickName; 
        }
        
        return nickName;
    }
    
    @SuppressLint("NewApi")
	public void setUserInfo(Context context, final  Map<String,String> info,final Map<String,String> infoExtra,final OnUserInfoCallback onUserInfoCallback) {
        String udeskId = generateUdeskIdForServer(context);
        String nickName = generateUdeskNickNameForServer(context);
        if(UDEnvConstants.isDebugMode) {
            Log.e(TAG, "   setUserInfo   udeskId:"+udeskId );
        }

        UDUserInfoTask task=new UDUserInfoTask(context,onUserInfoCallback);
        task.setUserInfo(info, infoExtra);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
        	task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,subDomainName, secretKey, udeskId ,nickName);
        }else{
        	task.execute ( subDomainName, secretKey, udeskId,nickName );
        }
    }

}
