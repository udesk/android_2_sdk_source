/**
 * 工具
 * @author xutao
 */
package cn.udesk.saas.sdk.utils;

import java.io.File;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeMap;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.util.Log;

public class UDUtils {

    /**
     * 获取imei号
     *
     * @param ctx
     * @return
     */
    public static String getImei(Context ctx) {
        String imei = "";
        try {
            TelephonyManager telephonyManager = (TelephonyManager) ctx
                    .getSystemService(Context.TELEPHONY_SERVICE);
            imei = telephonyManager.getDeviceId();

            if (null == imei) {
                imei = "";
            }
        } catch (Exception ex) {
            if(UDEnvConstants.isDebugMode) {
                Log.e("UDUtils", ex.toString());
            }
        }
        return imei;
    }

    /**
     *
     * @param s
     * @return
     */
    public static String MD5(byte[] btInput) {
        char hexDigits[] = {
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'a', 'b', 'c', 'd', 'e', 'f' };
        try {
            // 获得MD5摘要算法的 MessageDigest 对象
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            // 使用指定的字节更新摘要
            mdInst.update(btInput);
            // 获得密文
            byte[] md = mdInst.digest();
            // 把密文转换成十六进制的字符串形式
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     *
     * @param s
     * @return
     */
    public static String MD5(String s) {
        return MD5(s.getBytes());
    }


    /**
     * 获取手机的Mac地址
     * @return
     */
    public static String getLocalMacAddress(Context context) {
        String macAddress = "";
        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifi.getConnectionInfo();
        if(info != null) {
            macAddress = info.getMacAddress();
        }
        return macAddress;
    }


    /**
     * 获取SDcard可用空间大小
     * @return
     */
    public static long getSDCardFreeSize() {
        // 取得SD卡文件路径
        File path = Environment.getExternalStorageDirectory();
        StatFs sf = new StatFs(path.getPath());
        // 获取单个数据块的大小(Byte)
        long blockSize = sf.getBlockSize();
        // 空闲的数据块的数量
        long freeBlocks = sf.getAvailableBlocks();
        // 返回SD卡空闲大小
        return freeBlocks * blockSize; //单位Byte
    }


    /**
     *
     * @param context
     * @param key
     * @return
     */
    public static String loadConfigString(Context context, String key) {
        SharedPreferences sp = context.getSharedPreferences(
                UDEnvConstants.PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sp.getString(key, null);
    }


    /**
     *
     * @param context
     * @param key
     * @param value
     * @return
     */
    public static boolean saveConfigString(Context context, String key, String value) {
        SharedPreferences sp = context.getSharedPreferences(
                UDEnvConstants.PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sp.edit().putString(key, value).commit();
    }


    /**
     * Create a file Uri for saving an image or video
     */
    public static Uri getOutputMediaFileUri() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return Uri.fromFile(getOutputMediaFile("IMG_" + timeStamp + ".jpg"));
    }


    /**
     * Create a File for saving an image or video
     */
    public static File getOutputMediaFile(String mediaName) {
        File mediaStorageDir = null;
        try {
            mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "UDesk");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        File mediaFile = new File(mediaStorageDir.getPath() + File.separator + mediaName);
        return mediaFile;
    }


	public static String generateSign(  String secretKey, String uid, HashMap<String, String> otherParams) {
	//	uid = uid + "_" + subDomain;

		StringBuilder builder = new StringBuilder();
		if (otherParams == null || otherParams.isEmpty()) {
			//builder.append("user_id=").append(uid).append("&");// TODO  后续user_id要修改为常量值  20150504
		} else {
			if (!otherParams.containsKey("user_id")) {
				otherParams.put("user_id", uid);
			}

			if (otherParams.size() > 1) {
				TreeMap<String, String> tm = new TreeMap<String, String>( otherParams);
				Set<String> keySet = tm.keySet();
				for (String key : keySet) {
					builder.append(key).append(tm.get(key)).append("&");
				}
			}
		}

		String signParams =   builder.toString() + secretKey;
		return UDUtils.MD5(signParams);
	}

}
