/**
 * IM聊天内容
 * @author xutao
 */
package cn.udesk.saas.sdk.chat;

import cn.udesk.saas.sdk.db.UDIMDBHelper;

public class UDIMMessage {

    // 是否是自己发布的消息
    public final static int TYPE_MINE  = 0x01;
    // 是否是文字
    public final static int TYPE_TEXT  = 0x02;
    // 是否是图片
    public final static int TYPE_IMAGE = 0x04;
    // 是否是时间显示
    public final static int TYPE_TIME  = 0x08;

    public int type = 0;

    // 消息文本，如果是图片，text为imageURL
    public String text_url = "";

    // 图片
    public String thumbnailPath = "";
    
    public long id=-1;
    
    public int state=UDIMDBHelper.RESULT_WAIT;

}
