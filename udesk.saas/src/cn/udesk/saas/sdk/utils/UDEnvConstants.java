/**
 * 环境配置
 * @author xutao
 */
package cn.udesk.saas.sdk.utils;

public class UDEnvConstants {

    /**
     * 是否为调试模式
     */
    public   static boolean isDebugMode = false;
    static{
    	try {
			Class.forName("cn.udesk.saas.sdk.utils.UdeskDebug");
			isDebugMode =true;
		} catch ( Exception e) {
			isDebugMode =false;
		}
    }

    /**
     * 七牛上传图片连接
     */
    public final static String UD_QINIU_UPLOAD_IMAGE = "http://7sbqa9.com2.z0.glb.qiniucdn.com/";

    /**
     * 本地 prefrences
     */
    public final static String PREF_FILE_NAME  = "udesk_pref";
    public final static String PREF_LAST_CS    = "udesk_last_cs";


}
