/**
 * 聊天列表
 *
 * @author xutao
 */
package cn.udesk.saas.sdk.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.udesk.saas.sdk.ResourcesIDLoader;
import cn.udesk.saas.sdk.chat.UDIMMessage;
import cn.udesk.saas.sdk.utils.UDLruCache;
import cn.udesk.saas.sdk.view.UdeskImStateView;

public class UDIMChatAdatper extends BaseAdapter {

    private Context mContext;
    private ArrayList<UDIMMessage> list = new ArrayList<UDIMMessage>();

    private UDLruCache<String, Bitmap> lruCache;

    public UDIMChatAdatper(Context context) {
        mContext = context;
        initResId(mContext);
        lruCache = new UDLruCache<String, Bitmap>(20);
    }

    private void initResId(Context context){
    	ResourcesIDLoader idLoader = ResourcesIDLoader.getResIdLoaderInstance(mContext) ;
        LAYOUT_ID_ADAPTER = idLoader.getResLayoutID("udesk_layout_im_item");
        
        ID_udesk_im_item_time= idLoader.getResIdID("udesk_im_item_time");
        ID_udesk_im_item_my = idLoader.getResIdID("udesk_im_item_my");
        ID_udesk_im_my_message = idLoader.getResIdID("udesk_im_my_message");
        ID_udesk_im_my_image=idLoader.getResIdID("udesk_im_my_image");

        ID_udesk_im_item_cs= idLoader.getResIdID("udesk_im_item_cs");
        ID_udesk_im_cs_message= idLoader.getResIdID("udesk_im_cs_message");
        ID_udesk_im_cs_image= idLoader.getResIdID("udesk_im_cs_image");
        
        ID_udesk_im_my_state = idLoader.getResIdID("udesk_im_state");
    }
    @Override
    public int getCount() {
        return list.size();
    }

    /**
     * 添加历史纪录
     * @param messages
     */
    public void addHistoryArray(ArrayList<UDIMMessage> messages) {
        if(messages == null) {
            return;
        }
        for(int i = messages.size() - 1; i >= 0 ; i --) {
            list.add(0, messages.get(i));
            addLocalBitmap(messages.get(i));
        }
        notifyDataSetChanged();
    }

    /**
     * 添加新的聊天记录
     * @param message
     */
    public void addItem(UDIMMessage message) {
        if(message == null) {
            return;
        }
        list.add(message);
        addLocalBitmap(message);
        notifyDataSetChanged();
    }

    @Override
    public UDIMMessage getItem(int position) {
        if(position < 0 || position >= list.size()) {
            return null;
        }
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //优化ListView
        ItemViewCache viewCache;

        if(convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(LAYOUT_ID_ADAPTER, null);
            viewCache = new ItemViewCache();

            viewCache.mTime = (TextView)convertView.findViewById(ID_udesk_im_item_time);

            viewCache.mMyItem = (LinearLayout)convertView.findViewById(ID_udesk_im_item_my);
            viewCache.mMyText = (TextView)convertView.findViewById(ID_udesk_im_my_message);
            viewCache.mMyImage = (ImageView)convertView.findViewById(ID_udesk_im_my_image);
            viewCache.mStateView = (UdeskImStateView)convertView.findViewById(ID_udesk_im_my_state);

            viewCache.mCSItem = (LinearLayout)convertView.findViewById(ID_udesk_im_item_cs);
            viewCache.mCSText = (TextView)convertView.findViewById(ID_udesk_im_cs_message);
            viewCache.mCSImage = (ImageView)convertView.findViewById(ID_udesk_im_cs_image);

            convertView.setTag(viewCache);
        } else {
            viewCache = (ItemViewCache)convertView.getTag();
        }

        UDIMMessage message = list.get(position);
        viewCache.message = message;

        if((message.type & UDIMMessage.TYPE_TIME) == UDIMMessage.TYPE_TIME) {
            viewCache.mMyItem.setVisibility(View.GONE);
            viewCache.mCSItem.setVisibility(View.GONE);

            viewCache.mTime.setVisibility(View.VISIBLE);
            viewCache.mTime.setText(message.text_url);
        } else {

            viewCache.mTime.setVisibility(View.GONE);

            if((message.type & UDIMMessage.TYPE_MINE) == UDIMMessage.TYPE_MINE) {
                viewCache.mMyItem.setVisibility(View.VISIBLE);
                viewCache.mCSItem.setVisibility(View.GONE);
                if((message.type & UDIMMessage.TYPE_TEXT) == UDIMMessage.TYPE_TEXT) {
                    viewCache.mMyText.setText(UDEmojiAdapter.replaceEmoji(mContext, message.text_url));
                    viewCache.mMyText.setVisibility(View.VISIBLE);
                    viewCache.mMyImage.setVisibility(View.GONE);
                } else {
                    viewCache.mMyImage.setImageBitmap(lruCache.get(message.thumbnailPath));
                    viewCache.mMyText.setVisibility(View.GONE);
                    viewCache.mMyImage.setVisibility(View.VISIBLE);
                }

                viewCache.mStateView.changeUiState(message.state);
            } else {
                viewCache.mMyItem.setVisibility(View.GONE);
                viewCache.mCSItem.setVisibility(View.VISIBLE);
                if((message.type & UDIMMessage.TYPE_TEXT) == UDIMMessage.TYPE_TEXT) {
                    viewCache.mCSText.setText(UDEmojiAdapter.replaceEmoji(mContext, message.text_url));
                    viewCache.mCSText.setVisibility(View.VISIBLE);
                    viewCache.mCSImage.setVisibility(View.GONE);
                } else {
                    viewCache.mCSImage.setImageBitmap(lruCache.get(message.thumbnailPath));
                    viewCache.mCSText.setVisibility(View.GONE);
                    viewCache.mCSImage.setVisibility(View.VISIBLE);
                }
            }
        }


        return convertView;
    }


    //元素的缓冲类, 用于优化ListView
    private static class ItemViewCache {
        public LinearLayout mMyItem, mCSItem;
        public TextView mMyText, mCSText;
        public ImageView mMyImage, mCSImage;
        public TextView mTime;
        public UdeskImStateView mStateView;
        UDIMMessage message;
    }


    private void addLocalBitmap(UDIMMessage message) {
        if((message.type & UDIMMessage.TYPE_IMAGE) == UDIMMessage.TYPE_IMAGE) {
            if(!TextUtils.isEmpty(message.thumbnailPath)) {
                lruCache.put(message.thumbnailPath, BitmapFactory.decodeFile(message.thumbnailPath));
            }
        }
    }

    public boolean changeImState(View convertView,long id ){
		  Object tag = convertView.getTag();
		  if(tag!=null && tag instanceof ItemViewCache){
			  ItemViewCache cache = (ItemViewCache)tag;
			  if(id==cache.message.id){
				  cache.mStateView.changeUiState(cache.message.state);
				  return true;
			  }
		  }
		  
		  return false;
    }
    
    public TextView getTextViewForMyOrCs(View convertView ){
		  Object tag = convertView.getTag();
		  if(tag!=null && tag instanceof ItemViewCache){
			  ItemViewCache cache = (ItemViewCache)tag;
		    	if((cache.message.type & UDIMMessage.TYPE_TEXT) != UDIMMessage.TYPE_TEXT){
		    		throw new RuntimeException("  we need text type ");
		    	}
		    	
			  if((cache.message.type & UDIMMessage.TYPE_MINE) == UDIMMessage.TYPE_MINE) {
				  return cache.mMyText;
			  }else{
				  return cache.mCSText;
			  }
		  }
		  
		  return null;
    }

    Rect mHitRect = new Rect();
    public boolean isRetryClick(View parentView,int[] xy ){
		  Object tag = parentView.getTag();
		  if(tag!=null && tag instanceof ItemViewCache){
			  ItemViewCache cache = (ItemViewCache)tag;
			  cache.mStateView.getHitRect(mHitRect);
			  return mHitRect.contains(xy[0]-parentView.getLeft()-cache.mMyItem.getLeft(), xy[1]-parentView.getTop()-cache.mMyItem.getTop());
		  }

		  return false;
    }

    //TODO 这些变量或许设置为static 更合适
    private   int  LAYOUT_ID_ADAPTER  ;
    private   int  ID_udesk_im_item_time ;
    private   int  ID_udesk_im_item_my ;
    private   int  ID_udesk_im_my_message ;
    private   int  ID_udesk_im_my_image ;
    private   int  ID_udesk_im_item_cs ;
    private   int  ID_udesk_im_cs_message ;
    private   int  ID_udesk_im_cs_image ;
    private   int  ID_udesk_im_my_state ;
}
