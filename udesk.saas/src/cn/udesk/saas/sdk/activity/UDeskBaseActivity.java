package cn.udesk.saas.sdk.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.text.TextUtils;
import cn.udesk.saas.sdk.ResourcesIDLoader;

@SuppressLint("NewApi")
public class UDeskBaseActivity extends Activity {

    static final String FRAGMENTS_TAG = "fragment_name";
    static final String DATA_TAG = "__data";//用"__data"跟一般Intent中的"data"进行区分.

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		mResIDLoader = ResourcesIDLoader.getResIdLoaderInstance(this);
		super.onCreate(savedInstanceState);

		String fragmentName = this.getIntent() .getStringExtra(FRAGMENTS_TAG);
		if (savedInstanceState == null && !TextUtils.isEmpty(fragmentName)) {

			Bundle args = null;
			if (this.getIntent() != null) {
				Bundle source = getIntent().getBundleExtra(DATA_TAG);
				if (source != null) {
					args = new Bundle(source);
				}
			}
			Fragment mainFragment = Fragment.instantiate(this, fragmentName, args);
			if(  !(mainFragment instanceof UDeskFragment)){
			    throw new RuntimeException(" your Fragment must be a subclass of "+UDeskFragment.class.getName());
			}
			
			this.getFragmentManager().beginTransaction()
					.add(android.R.id.content, mainFragment)
					.commitAllowingStateLoss();
		}
	}

    @Override
   public void onBackPressed() {
        Fragment mainFragment = getFragmentManager().findFragmentById(android.R.id.content);
       if(mainFragment!=null &&  ( (UDeskFragment)mainFragment).onBackPressed()){
           return;
       }
       super.onBackPressed();
   }
    
	protected ResourcesIDLoader mResIDLoader;
}
