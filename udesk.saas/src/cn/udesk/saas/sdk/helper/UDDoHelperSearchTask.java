/**
 * 搜索任务
 * @author xutao
 */
package cn.udesk.saas.sdk.helper;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import cn.udesk.saas.sdk.utils.UDEnvConstants;
import cn.udesk.saas.sdk.utils.UDHttpUtil;
import cn.udesk.saas.sdk.utils.UDUtils;

public class UDDoHelperSearchTask extends AsyncTask<String, Integer, String> {

    private OnDoHelperSearchListener mListener;
    private boolean isRunning = true;


    public UDDoHelperSearchTask(OnDoHelperSearchListener listener) {
        this.mListener = listener;
        isRunning = true;
    }

    public void cancelled() {
        isRunning = false;
        cancel(true);
    }

    @Override
    protected String doInBackground(String... params) {

        String subDomain = params[0];
        String secretKey = params[1];
        String query = params[2];

        String url = subDomain;
        if(!subDomain.startsWith("http://")) {
            url = "http://" + subDomain;
        }

        String signParams = "q=" + query + "&" + secretKey;
        url = url + "/api/v1/articles/search.json?q=" + query + "&sign=" + UDUtils.MD5(signParams);

        if(UDEnvConstants.isDebugMode) {
            Log.w("UDGetKownlegeListTask", url);
        }

        return UDHttpUtil.get(url);
    }


    @Override
    protected void onPostExecute(String result) {
        if(UDEnvConstants.isDebugMode) {
            Log.w("UDGetKownlegeListTask", "result = " + result);
        }

        if(!isRunning) {
            return;
        }

        try {
            if(TextUtils.isEmpty(result)) {
                mListener.onFail(null);
                return;
            }

            JSONObject json = new JSONObject(result);
            int status = json.optInt("status");

            if(status == 0) {
                if(json.has("contents")) {
                    JSONArray kownlegeArray = json.optJSONArray("contents");
                    if(kownlegeArray != null && kownlegeArray.length() > 0) {
                        UDHelperItem[] array = new UDHelperItem[kownlegeArray.length()];
                        for(int i = 0; i < kownlegeArray.length(); i ++) {
                            JSONObject data = kownlegeArray.optJSONObject(i);
                            array[i] = new UDHelperItem();
                            array[i].id = data.optInt("id");
                            array[i].subject = data.optString("subject");
                        }
                        mListener.onSucess(array);
                        return;
                    }
                }
                mListener.onSucess(null);
            } else {
                String errorMessage = json.optString("message");
                mListener.onFail(errorMessage);
            }

        } catch(Exception e) {
            mListener.onFail(null);
        }
    }


    public interface OnDoHelperSearchListener {

        public void onSucess(UDHelperItem[] items);

        public void onFail(String result);

    }


}
