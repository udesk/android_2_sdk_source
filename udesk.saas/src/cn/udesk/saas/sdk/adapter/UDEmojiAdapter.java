/**
 * 表情适配器
 *
 * @author xutao
 */
package cn.udesk.saas.sdk.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import cn.udesk.saas.sdk.ResourcesIDLoader;

public class UDEmojiAdapter extends BaseAdapter {

    public final static String EMOJI_PREFIX = "[emoji";

    public final static String[] EMOJI_ARRAY = {
        "001", "002", "003", "004", "005", "006", "007",
        "008", "009", "010", "011", "012", "013", "014",
        "015", "016", "017", "018", "019", "020", "021",
        "022", "023", "024", "025", "026", "027", "028",
    };

    public   static int[] EMOJI_RESOURCE_ID_ARRAY = null;

    private   int  LAYOUT_ID_ADAPTER = 0;

    private Context mContext;

    public UDEmojiAdapter(Context context) {
        mContext = context;
        init(mContext);
    }
    
    private void init(Context context){
        ResourcesIDLoader idLoader =  ResourcesIDLoader.getResIdLoaderInstance(context);
        LAYOUT_ID_ADAPTER = idLoader.getResLayoutID("udesk_layout_emoji_item");
        
        EMOJI_RESOURCE_ID_ARRAY = new  int[] {
                idLoader.getResDrawableID("udesk_001"), idLoader.getResDrawableID("udesk_002"), idLoader.getResDrawableID("udesk_003"), idLoader.getResDrawableID("udesk_004"), idLoader.getResDrawableID("udesk_005"), idLoader.getResDrawableID("udesk_006"), idLoader.getResDrawableID("udesk_007"),
                idLoader.getResDrawableID("udesk_008"), idLoader.getResDrawableID("udesk_009"), idLoader.getResDrawableID("udesk_010"), idLoader.getResDrawableID("udesk_011"), idLoader.getResDrawableID("udesk_012"), idLoader.getResDrawableID("udesk_013"), idLoader.getResDrawableID("udesk_014"),
                idLoader.getResDrawableID("udesk_015"), idLoader.getResDrawableID("udesk_016"), idLoader.getResDrawableID("udesk_017"), idLoader.getResDrawableID("udesk_018"), idLoader.getResDrawableID("udesk_019"), idLoader.getResDrawableID("udesk_020"), idLoader.getResDrawableID("udesk_021"),
                idLoader.getResDrawableID("udesk_022"), idLoader.getResDrawableID("udesk_023"), idLoader.getResDrawableID("udesk_024"), idLoader.getResDrawableID("udesk_025"), idLoader.getResDrawableID("udesk_026"), idLoader.getResDrawableID("udesk_027"), idLoader.getResDrawableID("udesk_028")
            };
    }

    @Override
    public int getCount() {
        return EMOJI_ARRAY.length;
    }

    @Override
    public String getItem(int position) {
        if(position < 0 || position >= EMOJI_ARRAY.length) {
            return null;
        }
        return "[emoji" + EMOJI_ARRAY[position] + "]";
    }

    public int getItemResourceId(int position) {
        if(position < 0 || position >= EMOJI_ARRAY.length) {
            return 0;
        }
        return EMOJI_RESOURCE_ID_ARRAY[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //优化ListView
        if(convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(LAYOUT_ID_ADAPTER, null);
        }

        ((ImageView)convertView).setImageResource(EMOJI_RESOURCE_ID_ARRAY[position]);

        return convertView;
    }



    public static SpannableString replaceEmoji(Context mContext, String text) {
        final int emojiWidth = (int)(mContext.getResources().getDisplayMetrics().density * 40 / 2.0f);

        final int prefixLength = EMOJI_PREFIX.length();
        int index = 0, start = 0;
        SpannableString spannable = new SpannableString(text);

        index = text.indexOf(EMOJI_PREFIX, index);
        while(index > -1) {
            start = index + prefixLength;
            String emojiNumber = text.substring(start, text.indexOf("]", start));
            for(int j = 0; j < EMOJI_ARRAY.length; j ++) {
                if(EMOJI_ARRAY[j].equals(emojiNumber)) {
                    Drawable drawable = mContext.getResources().getDrawable(EMOJI_RESOURCE_ID_ARRAY[j]);
                    if (drawable != null) {
                        drawable.setBounds(0, 0, emojiWidth, emojiWidth);
                        ImageSpan span = new ImageSpan(drawable, ImageSpan.ALIGN_BOTTOM);
                        spannable.setSpan(span, index, start + emojiNumber.length() + 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                        break;
                    }
                }
            }

            index = text.indexOf(EMOJI_PREFIX, index + 7);
        }

        return spannable;
    }

}
